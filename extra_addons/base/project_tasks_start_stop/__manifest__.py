# -*- coding: utf-8 -*-
#╔══════════════════════════════════════════════════════════════════╗
#║                                                                  ║
#║                ╔═══╦╗       ╔╗  ╔╗     ╔═══╦═══╗                 ║
#║                ║╔═╗║║       ║║ ╔╝╚╗    ║╔═╗║╔═╗║                 ║
#║                ║║ ║║║╔╗╔╦╦══╣╚═╬╗╔╬╗ ╔╗║║ ╚╣╚══╗                 ║
#║                ║╚═╝║║║╚╝╠╣╔╗║╔╗║║║║║ ║║║║ ╔╬══╗║                 ║
#║                ║╔═╗║╚╣║║║║╚╝║║║║║╚╣╚═╝║║╚═╝║╚═╝║                 ║
#║                ╚╝ ╚╩═╩╩╩╩╩═╗╠╝╚╝╚═╩═╗╔╝╚═══╩═══╝                 ║
#║                          ╔═╝║     ╔═╝║                           ║
#║                          ╚══╝     ╚══╝                           ║
#║ SOFTWARE DEVELOPED AND SUPPORTED BY ALMIGHTY CONSULTING SERVICES ║
#║                   COPYRIGHT (C) 2016 - TODAY                     ║
#║                   http://www.almightycs.com                      ║
#║                                                                  ║
#╚══════════════════════════════════════════════════════════════════╝
{
    'name': 'Project Task Start Stop',
    'version': '1.0.3',
    'author' : 'Almighty Consulting Services',
    'website' : 'http://turkehpatel.odoo.com',
    'summary': """Project Task Start Stop functionality.""",
    'description': """Project Task Start Stop improvements to simplify project task usability
    start stop tasks task timer time traking
    """,
    'depends': ['project', 'hr_timesheet'],
    'category': 'Project Management',
    'data': [
        'views/project_view.xml',
        'views/project_template.xml',
    ],
    'images': [
        'static/description/tasks_start_stop_kanban_cover_almightycs.png',
    ],
    'installable': True,
    'auto_install': False,
    'price': 22,
    'currency': 'EUR',
}
