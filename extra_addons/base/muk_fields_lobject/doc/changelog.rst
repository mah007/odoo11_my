`2.0.0`
-------

- Migrated to Python 3
- Can handle Base64 input
- Can return value as Base64
- Can return value as Checksum

`1.0.0`
-------

- Init version
