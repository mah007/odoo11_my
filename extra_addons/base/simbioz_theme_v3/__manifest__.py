{
    'name': 'Simbioz Theme v3',
    'version': '11.0.1.0.3',
    "author": "Vitaliy Demchenko - Simbioz Holding",
    "website": "https://github.com/forij",
    "category": "Simbioz Tools",
    'sequence': 10,
    'images': [],
    'depends': [
        "base",
        "web",
    ],
    'data': [
        "views/link.xml",
        "views/menu.xml"
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'qweb': [
    ],
}
