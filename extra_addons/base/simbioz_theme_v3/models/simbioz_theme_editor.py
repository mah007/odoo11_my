# -*- coding: utf-8 -*-

from odoo import api, fields, models
import logging
import os

_logger = logging.getLogger(__name__)

class SimbiozCustomCSS(models.Model):
    _name = 'simbioz.custom_css'

    main_css = fields.Boolean()
    active_css = fields.Boolean(string="active", default=True)
    name = fields.Char(string="Name")
    code_stylus = fields.Text(string="Stylus")
    code_css = fields.Text(string="Stylus")

    def compouse_css(self):
        main_css = self.env['simbioz.custom_css'].search([('main_css', '=', True)])
        main_css['code_stylus'] = '/* Stylus v0.54.5 */ \n\n'
        for r in self.env['simbioz.custom_css'].search([('active_css', '=', True), ('main_css', '!=', True)]):
            header = '/*  ' + r['name'] + '  */' + '\n\n'
            if(r['code_stylus'] == False):
                main_css['code_stylus'] += header
            else:
                main_css['code_stylus'] += header + r['code_stylus'] + '\n\n'
        
        # path = os.path.dirname(os.path.abspath(__file__))[0:-6] + 'static/src/less/main.less'
        # file = open(path,'w') 
        # file.write(main_css['code_stylus']) 
        # file.close()
        # _logger.info('<---------------------file----------------->')
        # _logger.info(os.path.dirname(os.path.abspath(__file__)))

    @api.model
    def get_css(self):
        return self.env['simbioz.custom_css'].search([('main_css', '=', True)])['code_stylus']

    @api.model
    def create(self, vals):
        res = super(SimbiozCustomCSS, self).create(vals)
        _logger.info('<----------Create-------------->')
        self.compouse_css()
        return res

    @api.multi
    def unlink(self):
        res = super(SimbiozCustomCSS, self).unlink()
        self.compouse_css()
        return res
    

    @api.multi
    def write(self, vals):
        res = super(SimbiozCustomCSS, self).write(vals)
        _logger.info('<-----------Write------------->')
        if( self['main_css'] != True ):
            self.compouse_css()
        return res