odoo.define('simbioz_theme_v3.ace_widget', function (require) {
    "use strict";

    var FieldText = require('web.basic_fields').FieldText;
    var fieldRegistry = require('web.field_registry');

    var SimbiozAce = FieldText.extend({
        _renderReadonly: function(){
            this._super.apply(this, arguments);
            this.$el["0"].id = _.uniqueId('ace_');
            setTimeout( ()=>{
                const editor = ace.edit(this.$el["0"].id);
                editor.setTheme("ace/theme/monokai");
                editor.session.setMode("ace/mode/" + this.nodeOptions.mode);
                editor.setOptions({
                    maxLines: Infinity,
                    enableBasicAutocompletion: true,
                    enableLiveAutocompletion: true,
                    enableSnippets: true,
                    showLineNumbers: true,
                    fontSize: "12pt"
                });
            }, 100);    
        },
        _renderEdit: function () {
            this._super.apply(this, arguments);
            setTimeout( ()=>{
                const editor = ace.edit(this.$input["0"].id);
                editor.setTheme("ace/theme/monokai");
                editor.session.setMode("ace/mode/" + this.nodeOptions.mode);
                editor.setOptions({
                    maxLines: Infinity,
                    enableBasicAutocompletion: true,
                    enableLiveAutocompletion: true,
                    enableSnippets: true,
                    showLineNumbers: true,
                    fontSize: "12pt"
                });
                editor.getSession().on('change', ()=>{
                    console.log(this);
                    this.$input["0"].value = editor.getSession().getValue();
                    this.$input.first().trigger('change');
                });
            }, 100);    
        },
    });

    fieldRegistry.add('ace', SimbiozAce);

});