odoo.define('simbioz_theme_v3.loader', function (require) {
    "use strict";

    const rpc = require('web.rpc');

    rpc.query({
        model: 'simbioz.custom_css',
        method: 'get_css',
    }).then(function(res) {
        $('head').append( 
            $(`<style>
                ${res}
            </style>`) )
    });

});
