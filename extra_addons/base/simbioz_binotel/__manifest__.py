# -*- coding: utf-8 -*-
{
	'name': 'Simbioz Binotel',
	'version': '1.0',
	'category': 'Knowledge Management',
	'summary': 'CRM integration with binotel.ua for Odoo.',
	'author': 'Paul Marlow - Simbioz Holding',
	'website': 'http://simbioz.ua',
	'images': ['static/description/icon.png'],
	'depends': [
		'base',
		'crm',
		'crm_phonecall',
		# 'bus'
	],
	'data': [
		#'security/ir.model.access.csv',
		# 'views/binotel_config_settings_container_view.xml',
		'views/res_config_view.xml',
		'views/crm_phonecall_view.xml',
		'views/binotel_client_view.xml',
		'views/binotel_operator_view.xml',
		'views/res_partner_view.xml',
		# 'views/template.xml'
	],
	'application': True,
	'installable': True,
}