#coding: utf-8
# FROM ODOO11 START
# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
import datetime
import json
import os
import logging
import requests
import werkzeug.utils
import werkzeug.wrappers

from hashlib import md5
from itertools import islice
from xml.etree import ElementTree as ET

import odoo
from requests import HTTPError, ConnectionError, post

from odoo import http, models, fields, _, SUPERUSER_ID
from odoo.http import request
from odoo.tools import pycompat, OrderedSet
from odoo.addons.http_routing.models.ir_http import slug, _guess_mimetype
from odoo.addons.website.controllers.main import Website, QueryURL
from odoo.exceptions import Warning
from .. models.binotel_api import BinotelAPI
from .. models.res_config import BinotelSettings

_logger = logging.getLogger(__name__)
# FROM ODOO11 START END


# Pushing new call information:
class BinotelController(http.Controller):
	# Call event handler:
	@http.route('/binotel/push', type='http', auth='public',
		methods=['GET', 'POST'], csrf=False)
	def update_calls(self, **post):

		# Skipping glitch:
		if 'requestType' in post:

			# Preparing API:
			keyholder = request.env['binotel.config.settings.container']
			key_public, key_secret = keyholder.get_keys()

			# Updating calls only if call is done:
			BinotelSettings.update_calls(key_public, key_secret, request.env, {
				'url':    'stats/call-details',
				'params': {'generalCallID': [post['generalCallID']]}
			})

		# Done:
		return BinotelAPI.dump({'status':'success'})


	# Call information event handler:
	@http.route('/binotel/crmlink', type='http', auth='public',
		methods=['GET', 'POST'], csrf=False)
	def update_url(self, **post):
		_logger.info('++++++++++++++++++++++ 000000 post %s', post)
		# Skipping glitch:
		if 'requestType' in post:
			_logger.info('++++++++++++++++++++++ 1111111 post %s', post)
			# Customer data:
			if post['requestType'] == 'gettingCallSettings':

				# Preparing API:

				base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')

				# Searching for a client:
				ids = request.env['binotel.client'].sudo().search(
					[('phone', '=', post['srcNumber'])])

				# Making new client link:
				if not ids:
					odoo_url = ''.join([base_url,
						'/web#id=&view_type=form&model=binotel.client&menu_id=693&action=963'])
					string = 'Неизвестный клиент - перейти в Odoo'
					return BinotelAPI.dump({
						'customerData': {
							'linkToCrmUrl':   odoo_url,
							'linkToCrmTitle': string
						}
					})

				# Using existing client:
				else:
					client = ids
					client_url = ''.join([
						base_url, '/web#id=', str(client.id),
						'&view_type=form&model=binotel.client'
					])
					return BinotelAPI.dump({
						'customerData': {
							'name':           client.name,
							'description':    client.description,
							'linkToCrmUrl':   client_url,
							'linkToCrmTitle': 'Посмотреть в Odoo'
						}
					})

		# Done:
		return BinotelAPI.dump({'status':'success'})
