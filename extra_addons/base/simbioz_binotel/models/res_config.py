# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from . binotel_api import BinotelAPI
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)

# Main Binotel settings:
class BinotelSettings(models.TransientModel):

	_inherit = 'res.config.settings'
	# Keys containers:
	default_key_secret = fields.Char(default_model='binotel.config.settings.container', string='Secret')
	default_key_public = fields.Char(default_model='binotel.config.settings.container', string='Key')

	# Updating view bus:
	@staticmethod
	def update_view(env, reason='update'):
		bus = env['bus.bus']
		message = {
			'subject': '',
			'body': reason,
			'mode': 'notify',
		}
		bus.sendone('simbioz_binotel', message)


	# Internal pool update:
	@staticmethod
	def update_model(env, name, records, search_id='id'):

		# Retrieving pool:
		pool = env[name]

		# Iterating over records to be made/updated:
		for record in records:

			# Checking existing records to update:
			ids = None
			if search_id in record:
				ids = pool.sudo().search([(search_id, '=', record[search_id])])
			if ids:
				ids.with_context(manual_sync=True).sudo().write(record)

			# Making new record if not found:
			else:
				pool.with_context(manual_sync=True).sudo().create(record)


	# Updating calls log:
	@staticmethod
	def update_calls(public, secret, env, query=None):

		# Making call:
		api = BinotelAPI(public, secret)
		if not query:
			result = [
				api.ocall('stats/all-incoming-calls-since', {
					'timestamp':1370034000
				}),
				api.ocall('stats/all-outgoing-calls-since', {
					'timestamp':1370034000
				})
			]

		# Making customized call:
		else:
			result = [api.ocall(query['url'], query['params'])]

		# Iterating over:
		call_records = []
		for calls in result:

			details = calls['callDetails'] if 'callDetails' in calls else []
			for call_id in details:
				call = details[call_id]

				# Naming and client relation:
				if type(call['customerData']) == dict:
					pool = env['binotel.client']
					internal_id = call['customerData']['id']
					ids = pool.sudo().search_read([('client_id', '=', internal_id)])
					desc = call['customerData']['name']
					client_id = ids[0]['id']
				else:
					desc = call['externalNumber']
					client_id = False

				# Call type and date:
				date = datetime.utcfromtimestamp(float(call['startTime']))
				if call['callType'] == '0':
					call_type = 'in'
				else:
					call_type = 'out'

				# Finalizing:
				call_records.append({
					'binotel_client_id': client_id,
					'call_id':           call['generalCallID'],
					'call_type':         call_type,
					'record_id':         call['callID'],
					'name':              desc,
					'partner_phone':     call['externalNumber'],
					'date':              date,
					'duration':          float(call['billsec'])/60,
					'disposition':       call['disposition'],
					'state':             'done'
				})

		# Updating calls view:
		BinotelSettings.update_view(env)

		# Applying changes to Odoo:
		BinotelSettings.update_model(env,
			'crm.phonecall', call_records, 'call_id')


	# Updating clients:
	def update_clients(self):

		# Making call:
		api = BinotelAPI(self.default_key_public, self.default_key_secret)
		clients = api.ocall('customers/list')

		# Iterating over clients:
		client_records = []
		for client_id in clients['customerData']:
			client = clients['customerData'][client_id]

			# Retrieving operator ID:
			employee = client['assignedToEmployeeID']
			oper = self.env['binotel.operator'].sudo().search_read(
				[('employee_id', '=', employee)])

			client_records.append({
				'operator_id': oper[0]['id'] if oper else None,
				'client_id':   client['id'],
				'name':        client['name'],
				'email':       client['email'],
				'phone':       client['numbers'][0]
			})

		# Updating Odoo records:
		BinotelSettings.update_model(self.env,
			'binotel.client', client_records, 'client_id')


	# Updating operators:
	def update_operators(self):

		# Making call:
		api = BinotelAPI(self.default_key_public, self.default_key_secret)
		opers = api.ocall('settings/list-of-employees')

		# Iterating over clients:
		oper_records = []
		if 'listOfEmployees' in opers:
			for oper_id in opers['listOfEmployees']:
				oper = opers['listOfEmployees'][oper_id]
				oper_records.append({
					'employee_id': int(oper['employeeID']),
					'name':        oper['name'],
					'email':       oper['email'],
					'status':      oper['extStatus']['status'],
					'line':        oper['extNumber']
				})

		# Updating Odoo records:
		BinotelSettings.update_model(self.env,
			'binotel.operator', oper_records, 'employee_id')

	# Wrappers:
	@api.multi
	def update_all(self):
		# Updating:
		public, secret = self.default_key_public, self.default_key_secret
		self.update_clients()
		self.update_operators()
		self.update_calls(public, secret, self.env)

		# Notifications:
		message = _('Database was succesfully synced')
		mess = {
			'title': _('Success!'),
			'message' : message
		}