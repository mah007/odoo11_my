# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

# Binotel Operators:
class BinotelOperator(models.Model):
	_name = 'binotel.operator'
	client_ids = fields.One2many('binotel.client', 'operator_id')
	employee_id = fields.Integer(readonly=True)
	user_id = fields.Many2one('res.users', string=_('Responsible'))
	email = fields.Char(readonly=True)
	name = fields.Char(readonly=True)
	line = fields.Integer(readonly=True)
	status = fields.Selection([
		('online', 'Online'),
		('offline', 'Offline'),
		('inuse', 'In-Use'),
		('ringing', 'Ringing'),
	], readonly=True)
