# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import Warning
from . call import Call
from . binotel_api import BinotelAPI
from . res_config import BinotelSettings

import logging

_logger = logging.getLogger(__name__)
# Binotel Clients log:
class BinotelClient(models.Model):
	_name = 'binotel.client'
	# Internal relations:
	call_ids = fields.One2many('crm.phonecall', 'binotel_client_id')
	partner_id = fields.Many2one('res.partner', string=_('Partner'))
	operator_id = fields.Many2one('binotel.operator', string=_('Operator'))
	client_id = fields.Char()

	# User values:
	name = fields.Char(required=True)
	phone = fields.Char(required=True)
	description = fields.Text(string=_('Description'))
	email = fields.Char()

	def get_client(self, record_id):
		records = self.env['binotel.client'].search([('id', '=', record_id.id)])
		return records.client_id

	def get_employee(self, vals):
		if 'operator_id' in vals:

			records = self.env['binotel.operator'].search(
				[('id', '=', vals['operator_id'])])
			if records:
				return records.line


	@api.model
	def create(self, vals):
		ctx = self.env.context
		if not 'manual_sync' in ctx:
			# Preparing API:
			keyholder = self.env['binotel.config.settings.container']
			key_public, key_secret = keyholder.get_keys()
			binotel = BinotelAPI(key_public, key_secret)

			# Processing basic values to match API requirements:
			vals['numbers'] = [vals['phone']]
			operator_line = self.get_employee(vals)
			if operator_line:
				vals['assignedToEmployeeNumber'] = operator_line

			# Removing trash information that may conflict with API:
			if 'client_id' in vals:
				del vals['client_id']

			# Creating client on Binotel database:
			result = binotel.ocall('customers/create', vals)
			vals['client_id'] = result['customerID']

			# Creating client in the internal Odoo database:
			client_id = super(BinotelClient, self).create(vals)

			# Retrieving client calls and updating client-call relations:

			BinotelSettings.update_calls(key_public, key_secret, self.env, {
				'url':    'stats/history-by-customer-id',
				'params': {'customerID': vals['client_id']}
			})
			return client_id
		else:
			return super(BinotelClient, self).create(vals)


	@api.multi
	def write(self, vals):
		ctx = self.env.context
		if not 'manual_sync' in ctx:

			# Preparing API:
			keyholder = self.env['binotel.config.settings.container']
			key_public, key_secret = keyholder.get_keys()
			binotel = BinotelAPI(key_public, key_secret)

			# Updating records:
			for record_id in self:
				vals['id'] = self.get_client(record_id)
				operator_line = self.get_employee(vals)
				if operator_line:
					vals['assignedToEmployeeNumber'] = operator_line
				result = binotel.ocall('customers/update', vals)

		return super(BinotelClient, self).write(vals)

	@api.multi
	def unlink(self):
		ctx = self.env.context
		# Preparing API:
		keyholder = self.env['binotel.config.settings.container']
		key_public, key_secret = keyholder.get_keys()
		binotel = BinotelAPI(key_public, key_secret)

		# Mass unlinking:
		for record_id in ids:

			# Removing client from Binotel database:
			client_id = self.get_client(record_id)
			result = binotel.ocall('customers/delete', {
				'customerID': client_id
			})

		return super(BinotelClient, self).unlink(context=ctx)

	def copy(self, *args, **kargs):
		raise Warning('Duplication is forbidden.')
	
	# Making a call:
	@api.multi
	def make_call(self):
		return Call.make_call_init(self)