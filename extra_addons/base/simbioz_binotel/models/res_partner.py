# -*- coding: utf-8 -*-
from odoo import api, fields, models

# Modifying partner to tie binotel clients:
class BinotelPartner(models.Model):
	_inherit = 'res.partner'
	binotel_ids = fields.One2many('binotel.client', 'partner_id')
