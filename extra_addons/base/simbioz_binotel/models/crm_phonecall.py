# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import Warning
from . call import Call
from . binotel_api import BinotelAPI

# Binotel Calls log:
class BinotelCall(models.Model):
	_inherit = 'crm.phonecall'
	binotel_client_id = fields.Many2one('binotel.client',
		string=_('Binotel Client'), readonly=True)
	call_type = fields.Selection([('in', _('In')), ('out', _('Out'))],
		readonly=True, string=_('Call Type'))
	call_id = fields.Char(readonly=True, string=_('Call ID'))
	record_id = fields.Char(readonly=True)
	duration = fields.Float(readonly=True, string=_('Duration'))
	date = fields.Datetime(readonly=True, string=_('Date'))
	partner_phone = fields.Char(readonly=True)
	disposition = fields.Selection([
		('ANSWER', 'Answered'),
		('TRANSFER', 'Transfered'),
		('ONLINE', 'Online'),
		('BUSY', 'Busy'),
		('NOANSWER', 'No Answer'),
		('CANCEL', 'Canceled'),
		('CONGESTION', 'Channel overflow'),
		('CHANUNAVAIL', 'SIP not available'),
		('VM', 'Voice mail'),
		('VM-SUCCESS', 'Voice mail success')
	], readonly=True, string=_('Status'))
	state = fields.Selection(default='done')

	# Downloading call record:
	@api.multi
	def download_record(self):

		# Preparing API:
		keyholder = self.env['binotel.config.settings.container']
		key_public, key_secret = keyholder.get_keys()
		binotel = BinotelAPI(key_public, key_secret)

		record = binotel.ocall('stats/call-record', {
			'callID':self.record_id,
		}, supress_internal_errors=True)

		# If no record found:
		if not 'url' in record:
			if self.duration > 0:
				raise Warning(
					_('Unknown error - can`t retrieve the record!'))
			else:
				raise Warning(
					_('Calls with zero duration have no records!'))

		# Returns record player action:
		return {
			'type': 'ir.actions.act_url',
			'url': record['url'],
			'nodestroy': True,
			'target': 'self'
		}

		# Javascript realization with HTML5 player, NYI:
		'''return {
			'type': 'ir.actions.client',
   			'tag': 'simbioz_binotel.download_record',
   			'target': 'new',
   			'params': {
   				'url': record['url']
   			}
		}'''


	# Making a call:
	@api.multi
	def make_call(self):
		return Call.make_call_init(self.binotel_client_id)