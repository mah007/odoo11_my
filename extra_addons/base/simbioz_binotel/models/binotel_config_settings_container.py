# -*- coding: utf-8 -*-
from odoo import api, fields, models

# Binotel settings container (used to save and load keys):
class BinotelSecure(models.Model):
	_name = 'binotel.config.settings.container'
	update_mode = fields.Boolean()
	key_secret = fields.Char()
	key_public = fields.Char()

	# Retrieving keys for API from other models:

	def get_keys(self):
		keys = self.default_get(['key_public', 'key_secret'])
		if 'key_public' in keys and 'key_secret' in keys:
			return (keys['key_public'], keys['key_secret'])
		else:
			raise Warning(_('Binotel keys was not properly set.'))