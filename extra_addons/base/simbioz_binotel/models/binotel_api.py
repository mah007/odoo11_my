#coding: utf-8
from odoo import _
from hashlib import md5
# PUSH handlers:
from openerp import http, SUPERUSER_ID
from openerp.http import request
from openerp.exceptions import Warning
# External dependencies:
from requests import HTTPError, ConnectionError, post
import json
import logging

_logger = logging.getLogger(__name__)

# Binotel API (internal use only):
class BinotelAPI:
	# API settings:
	api_host = 'https://api.binotel.com/api'
	api_version = '2.0'
	api_format = 'json'


	# Initializing keys:
	def __init__(self, public, secret, debug=True):
		self.debugging_enabled = debug
		self.key_public = public
		self.key_secret = secret


	# PHP-like JSON dump:
	@staticmethod
	def dump(params):
		if params == {}:
			return '[]'
		return json.dumps(params, separators=(',', ':'), sort_keys=True)


	# Debug messages:
	def log(self, text):
		if self.debugging_enabled:
			print('BINOTEL: ' + text)


	# API secret signature:
	def signature(self, params={}):
		# _logger.info('++++++++++++++++++++++ self.key_secret %s', self.key_secret)

		line = self.key_secret + self.dump(params)

		signature = md5(line.encode('utf-8')).hexdigest()
		self.log('Generated signature ' + signature)
		return signature


	# API call:
	def call(self, url, params={}):

		# Preparing public key and signature:
		params = dict(**params)
		params['signature'] = self.signature(params)
		params['key'] = self.key_public

		# Preparing and logging URL:
		parts = [self.api_host, self.api_version, url + '.' + self.api_format]
		url = '/'.join(parts)
		self.log('Making ' + self.api_format + ' request to ' + url + '...')

		# Preparing request:
		data = self.dump(params)
		headers = {
			'Content-Length': str(len(data)),
			'Content-Type':   'application/json'
		}

		# Making request, catching errors:
		response = post(url, headers=headers, data=data)
		if response.status_code != 200:
			raise HTTPError(response.status_code, response=response)

		# Finalizing:
		return response.json()


	# API call with validation for Odoo:
	def ocall(self, url, params={}, supress_internal_errors=False):
		try:
			answer = self.call(url, params)
			# _logger.info('++++++++++++++++++++++222222222222222222222 answer %s', answer)


		except HTTPError as error:
			code = error.response.status_code
			if code == 403:
				text = _('Access denied!') + ' ' + error.response.text
			else:
				text = _('Unexpected JSON request error') + ' ' + str(error.code) +'.'
			raise Warning(text)

		except ConnectionError as error:
			msg = _('Can`t conect to Binotel. Check service availability.')
			raise Warning(msg)

		# Checking status:
		if answer['status'] != 'success' and not supress_internal_errors:
			raise Warning(_('API error:') + ' ' + answer['message'])
		else:
			return answer