# -*- encoding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import Warning
from . res_config import BinotelSettings
from . binotel_api import BinotelAPI
import logging

_logger = logging.getLogger(__name__)

class Call(object):
    name = None

    # Establishing call connection:
    def make_call_init(self):

        # Retrieving user ID and checking access level:
        uid, op = self.env.user.id, self.operator_id
        user = self.env['res.users'].browse(uid)

        _logger.info('++++++++++++++++++++++ uid %s', uid)
        _logger.info('++++++++++++++++++++++ op %s', op)
        _logger.info('++++++++++++++++++++++ user %s', user)
        # Preparing API:
        keyholder = self.env['binotel.config.settings.container']
        key_public, key_secret = keyholder.get_keys()
        binotel = BinotelAPI(key_public, key_secret)
        line = None

        _logger.info('++++++++++++++++++++++ self %s', self)

        # Looking for a line:
        if op.user_id.id != uid:
            _logger.info('OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO')
            # Looking for an empty line:
            if user.has_group('sales_team.group_sale_manager'):
                operators = binotel.ocall('settings/list-of-employees')
                for operator_id in operators['listOfEmployees']:
                    operator = operators['listOfEmployees'][operator_id]
                    if 'extStatus' in operator:
                        if operator['extStatus']['status'] == 'online':
                            line = operator['extNumber']
                            break
                if not line:
                    raise Warning(_('No online line was found!'))

            # If not assigned and not a manager:
            else:
                text_raw = ''.join(
                    ['You have to be assigned to that client or to be '
                    ,'a sales manager to make a call.'])
                text = _(text_raw)
                raise Warning(text)
        else:
            line = op.line

        # Making a call:
        _logger.info('++++++++++++++++++++++ binotel %s', binotel)
        _logger.info('++++++++++++++++++++++ ext_number %s', str(line))
        _logger.info('++++++++++++++++++++++ phone_number %s', self.phone)
        try:
            call = binotel.ocall('calls/ext-to-phone', {
                'ext_number':    str(line),
                'phone_number':  str(self.phone)
            })
        # If operator is unavailable:
        # except Warning as error:
        #     return {
        #         'type': 'ir.actions.client',
        #         'tag': 'action_warn',
        #         'name': 'Binotel',
        #         'params': {
        #            'title': 'Warning!',
        #            'text': error.message,
        #         }
        #     }

        except:
            raise Warning(_('The client does not respond!'))

        # Updating calls list:
        finally:
            BinotelSettings.update_calls(key_public, key_secret, self.env, {
                'url':    'stats/history-by-number',
                'params': {'number': [str(self.phone)]}
            })