# -*- coding: utf-8 -*-
from . import res_config
from . import call
from . import binotel_api
from . import binotel_config_settings_container
from . import crm_phonecall
from . import binotel_client
from . import binotel_operator
from . import res_partner