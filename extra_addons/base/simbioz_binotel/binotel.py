#coding: utf-8
from openerp.tools.translate import _
from openerp.exceptions import Warning
from openerp import models, fields, api
from datetime import datetime
from hashlib import md5


# PUSH handlers:
from openerp import http, SUPERUSER_ID
from openerp.http import request


# External dependencies:
from requests import HTTPError, ConnectionError, post
import simplejson as json


# TODO:
# Turng string to _ translation.


# Binotel API (internal use only):
class BinotelAPI:

	# API settings:
	api_host = 'https://api.binotel.com/api'
	api_version = '2.0'
	api_format = 'json'


	# Initializing keys:
	def __init__(self, public, secret, debug=True):
		self.debugging_enabled = debug
		self.key_public = public
		self.key_secret = secret


	# PHP-like JSON dump:
	@staticmethod
	def dump(params):
		if params == {}:
			return '[]'
		return json.dumps(params, separators=(',', ':'), sort_keys=True)


	# Debug messages:
	def log(self, text):
		if self.debugging_enabled:
			print('BINOTEL: ' + text)


	# API secret signature:
	def signature(self, params={}):
		signature = md5(self.key_secret + self.dump(params)).hexdigest()
		self.log('Generated signature ' + signature)
		return signature


	# API call:
	def call(self, url, params={}):

		# Preparing public key and signature:
		params = dict(**params)
		params['signature'] = self.signature(params)
		params['key'] = self.key_public

		# Preparing and logging URL:
		parts = [self.api_host, self.api_version, url + '.' + self.api_format]
		url = '/'.join(parts)
		self.log('Making ' + self.api_format + ' request to ' + url + '...')

		# Preparing request:
		data = self.dump(params)
		headers = {
			'Content-Length': len(data),
			'Content-Type':   'application/json'
		}

		# Making request, catching errors:
		response = post(url, headers=headers, data=data)
		if response.status_code != 200:
			raise HTTPError(response.status_code, response=response)

		# Finalizing:
		return response.json()


	# API call with validation for Odoo:
	def ocall(self, url, params={}, supress_internal_errors=False):

		try: 
			answer = self.call(url, params)

		except HTTPError, error:
			code = error.response.status_code
			if code == 403:
				text = _('Access denied!') + ' ' + error.response.text
			else:
				text = _('Unexpected JSON request error') + ' ' + str(error.code) +'.'
			raise Warning(text)

		except ConnectionError, error:
			msg = _('Can`t conect to Binotel. Check service availability.')
			raise Warning(msg)

		# Checking status:
		if answer['status'] != 'success' and not supress_internal_errors:
			raise Warning(_('API error:') + ' ' + answer['message'])
		else:
			return answer


# Main Binotel settings:
class BinotelSettings(models.TransientModel):
	_inherit = 'res.config.settings'
	_name = 'binotel.config.settings'

	# Keys containers:
	default_key_secret = fields.Char(
		default_model='binotel.config.settings.container', string='Secret')
	default_key_public = fields.Char(
		default_model='binotel.config.settings.container', string='Key')


	# Updating view bus:
	@staticmethod
	def update_view(env, reason='update'):
		bus = env['bus.bus']
		message = {
			'subject': '',
			'body': reason,
			'mode': 'notify',
		}
		bus.sendone('simbioz_binotel', message)


	# Internal pool update:
	@staticmethod
	def update_model(env, name, records, search_id='id'):

		# Retrieving pool:
		pool = env[name]

		# Iterating over records to be made/updated:
		for record in records:
			
			# Checking existing records to update:
			ids = None
			if search_id in record:
				ids = pool.search([(search_id, '=', record[search_id])])
			if ids:
				ids.with_context(manual_sync=True).write(record)

			# Making new record if not found:
			else:
				pool.with_context(manual_sync=True).create(record)


	# Updating calls log:
	@staticmethod
	def update_calls(public, secret, env, query=None):

		# Making call:
		api = BinotelAPI(public, secret)
		if not query:
			result = [
				api.ocall('stats/all-incoming-calls-since', {
					'timestamp':1370034000
				}),
				api.ocall('stats/all-outgoing-calls-since', {
					'timestamp':1370034000
				})
			]

		# Making customized call:
		else:
			result = [api.ocall(query['url'], query['params'])]

		# Iterating over:
		call_records = []
		for calls in result:
			details = calls['callDetails'] if 'callDetails' in calls else []
			for call_id in details:
				call = details[call_id]
				
				# Naming and client relation:
				if type(call['customerData']) == dict:
					pool = env['binotel.client']
					internal_id = call['customerData']['id']
					ids = pool.search_read([('client_id', '=', internal_id)])
					desc = call['customerData']['name']
					client_id = ids[0]['id']
				else:
					desc = call['externalNumber']
					client_id = False

				# Call type and date:
				date = datetime.utcfromtimestamp(float(call['startTime']))
				if call['callType'] == '0':
					call_type = 'in'
				else:
					call_type = 'out'

				# Finalizing:
				call_records.append({
					'binotel_client_id': client_id,
					'call_id':           call['generalCallID'],
					'call_type':         call_type,
					'record_id':         call['callID'],
					'name':              desc,
					'partner_phone':     call['externalNumber'],
					'date':              date,
					'duration':          float(call['billsec'])/60,
					'disposition':       call['disposition'],
					'state':             'done'
				})

		# Updating calls view:
		BinotelSettings.update_view(env)

		# Applying changes to Odoo:
		BinotelSettings.update_model(env, 
			'crm.phonecall', call_records, 'call_id')


	# Updating clients:
	def update_clients(self):

		# Making call:
		api = BinotelAPI(self.default_key_public, self.default_key_secret)
		clients = api.ocall('customers/list')

		# Iterating over clients:
		client_records = []
		for client_id in clients['customerData']:
			client = clients['customerData'][client_id]

			# Retrieving operator ID:
			employee = client['assignedToEmployeeID']
			oper = self.env['binotel.operator'].search_read(
				[('employee_id', '=', employee)])

			client_records.append({
				'operator_id': oper[0]['id'] if oper else None,
				'client_id':   client['id'],
				'name':        client['name'],
				'email':       client['email'],
				'phone':       client['numbers'][0]
			})

		# Updating Odoo records:
		BinotelSettings.update_model(self.env, 
			'binotel.client', client_records, 'client_id')


	# Updating operators:
	def update_operators(self):

		# Making call:
		api = BinotelAPI(self.default_key_public, self.default_key_secret)
		opers = api.ocall('settings/list-of-employees')

		# Iterating over clients:
		oper_records = []
		if 'listOfEmployees' in opers:
			for oper_id in opers['listOfEmployees']:
				oper = opers['listOfEmployees'][oper_id]
				oper_records.append({
					'employee_id': int(oper['employeeID']),
					'name':        oper['name'],
					'email':       oper['email'],
					'status':      oper['extStatus']['status'],
					'line':        oper['extNumber']
				})

		# Updating Odoo records:
		BinotelSettings.update_model(self.env, 
			'binotel.operator', oper_records, 'employee_id')


	# Wrappers:
	@api.multi
	def update_all(self):

		# Updating:
		public, secret = self.default_key_public, self.default_key_secret
		self.update_clients()
		self.update_operators()
		self.update_calls(public, secret, self.env)

		# Notifications:
		return {
			'type': 'ir.actions.client',
			'tag': 'action_warn',
			'name': 'Binotel',
			'params': {
			   'title': 'Success!',
			   'text': 'Database was succesfully synced.',
			}
		}


# Binotel settings container (used to save and load keys):
class BinotelSecure(models.Model):
	_name = 'binotel.config.settings.container'
	update_mode = fields.Boolean()
	key_secret = fields.Char()
	key_public = fields.Char()

	# Retrieving keys for API from other models:
	@staticmethod
	def get_keys(cr):
		env = api.Environment(cr, 1, {})
		keyholder = env['binotel.config.settings.container']
		keys = keyholder.default_get(['key_public', 'key_secret'])
		if 'key_public' in keys and 'key_secret' in keys:
			return (keys['key_public'], keys['key_secret'])
		else:
			raise Warning(_('Binotel keys was not properly set.'))


# Establishing call connection:
def make_call(self):

	# Retrieving user ID and checking access level:
	uid, op = self.env.user.id, self.operator_id
	user = self.env['res.users'].browse(uid)

	# Preparing API:
	key_public, key_secret = BinotelSecure.get_keys(self.env.cr)
	binotel = BinotelAPI(key_public, key_secret)
	line = None

	# Looking for a line:
	if op.user_id.id != uid:

		# Looking for an empty line:
		if user.has_group('base.group_sale_manager'):
			operators = binotel.ocall('settings/list-of-employees')
			for operator_id in operators['listOfEmployees']:
				operator = operators['listOfEmployees'][operator_id]
				if 'extStatus' in operator:
					if operator['extStatus']['status'] == 'online':
						line = operator['extNumber']
						break
			if not line:
				raise Warning(_('No online line was found!'))

		# If not assigned and not a manager:
		else:
			text_raw = ''.join(
				['You have to be assigned to that client or to be ' 
				,'a sales manager to make a call.'])
			text = _(text_raw)
			raise Warning(text)
	else:
		line = op.line

	# Making a call:
	try:
		call = binotel.ocall('calls/ext-to-phone', {
			'ext_number':    str(line),
			'phone_number':  str(self.phone)
		})

	# If operator is unavailable:
	except Warning, error:
		return {
			'type': 'ir.actions.client',
			'tag': 'action_warn',
			'name': 'Binotel',
			'params': {
			   'title': 'Warning!',
			   'text': error.message,
			}
		}

	# Updating calls list:
	finally:
		BinotelSettings.update_calls(key_public, key_secret, self.env, {
			'url':    'stats/history-by-number',
			'params': {'number': [str(self.phone)]}
		})


# Binotel Clients log:
class BinotelClient(models.Model):
	_name = 'binotel.client'

	# Internal relations:
	call_ids = fields.One2many('crm.phonecall', 'binotel_client_id')
	partner_id = fields.Many2one('res.partner', string=_('Partner'))
	operator_id = fields.Many2one('binotel.operator', string=_('Operator'))
	client_id = fields.Char()

	# User values:
	name = fields.Char(required=True)
	phone = fields.Char(required=True)
	description = fields.Text(string=_('Description'))
	email = fields.Char()


	# Internal relations:
	def get_env(self, cr):
		return api.Environment(cr, 1, {})

	def get_client(self, cr, record_id):
		env = self.get_env(cr)
		records = env['binotel.client'].search_read([('id', '=', record_id)])
		return records[0]['client_id']

	def get_employee(self, cr, vals):
		if 'operator_id' in vals:
			env = self.get_env(cr)
			records = env['binotel.operator'].search_read(
				[('id', '=', vals['operator_id'])])
			if records:
				return records[0]['line']


	# Binotel Connection:
	def create(self, cr, uid, vals, context=None):
		if not 'manual_sync' in context:

			# Preparing API:
			key_public, key_secret = BinotelSecure.get_keys(cr)
			binotel = BinotelAPI(key_public, key_secret)

			# Processing basic values to match API requirements:
			vals['numbers'] = [vals['phone']]
			operator_line = self.get_employee(cr, vals)
			if operator_line:
				vals['assignedToEmployeeNumber'] = operator_line

			# Removing trash information that may conflict with API:
			if 'client_id' in vals: 
				del vals['client_id']

			# Creating client on Binotel database:
			result = binotel.ocall('customers/create', vals)
			vals['client_id'] = result['customerID']

			# Creating client in the internal Odoo database:
			client_id = super(BinotelClient, self).create(
				cr, uid, vals, context=context)

			# Retrieving client calls and updating client-call relations:
			env = self.get_env(cr)
			BinotelSettings.update_calls(key_public, key_secret, env, {
				'url':    'stats/history-by-customer-id',
				'params': {'customerID': vals['client_id']}
			})
			return client_id

		else:
			return super(BinotelClient, self).create(
				cr, uid, vals, context=context)

	def write(self, cr, uid, ids, vals, context=None):
		if not 'manual_sync' in context:
			
			# Preparing API:
			key_public, key_secret = BinotelSecure.get_keys(cr)
			binotel = BinotelAPI(key_public, key_secret)

			# Updating records:
			for record_id in ids:
				vals['id'] = self.get_client(cr, record_id)
				operator_line = self.get_employee(cr, vals)
				if operator_line:
					vals['assignedToEmployeeNumber'] = operator_line
				result = binotel.ocall('customers/update', vals)

		return super(BinotelClient, self).write(
			cr, uid, ids, vals, context=context)

	def unlink(self, cr, uid, ids, context=None):

		# Preparing API:
		key_public, key_secret = BinotelSecure.get_keys(cr)
		binotel = BinotelAPI(key_public, key_secret)

		# Mass unlinking:
		for record_id in ids:

			# Removing client from Binotel database:
			client_id = self.get_client(cr, record_id)
			result = binotel.ocall('customers/delete', {
				'customerID': client_id
			})

		return super(BinotelClient, self).unlink(
			cr, uid, ids, context=context)

	def copy(self, *args, **kargs):
		raise Warning('Duplication is forbidden.')


	# Making a call:
	@api.multi
	def make_call(self):
		return make_call(self)


# Modifying partner to tie binotel clients:
class BinotelPartner(models.Model):
	_inherit = 'res.partner'
	binotel_ids = fields.One2many('binotel.client', 'partner_id')


# Binotel Calls log:
class BinotelCall(models.Model):
	_inherit = 'crm.phonecall'
	binotel_client_id = fields.Many2one('binotel.client', 
		string=_('Binotel Client'), readonly=True)
	call_type = fields.Selection([('in', _('In')), ('out', _('Out'))], 
		readonly=True, string=_('Call Type'))
	call_id = fields.Char(readonly=True, string=_('Call ID'))
	record_id = fields.Char(readonly=True)
	duration = fields.Float(readonly=True, string=_('Duration'))
	date = fields.Datetime(readonly=True, string=_('Date'))
	partner_phone = fields.Char(readonly=True)
	disposition = fields.Selection([
		('ANSWER', 'Answered'),
		('TRANSFER', 'Transfered'),
		('ONLINE', 'Online'),
		('BUSY', 'Busy'),
		('NOANSWER', 'No Answer'),
		('CANCEL', 'Canceled'),
		('CONGESTION', 'Channel overflow'),
		('CHANUNAVAIL', 'SIP not available'),
		('VM', 'Voice mail'),
		('VM-SUCCESS', 'Voice mail success')
	], readonly=True, string=_('Status'))
	state = fields.Selection(default='done')

	# Downloading call record:
	@api.multi
	def download_record(self):

		# Making request for keys to configuration model:
		key_public, key_secret = BinotelSecure.get_keys(self.env.cr)

		# Retrieving call record URL:
		binotel = BinotelAPI(key_public, key_secret)
		record = binotel.ocall('stats/call-record', {
			'callID':self.record_id,
		}, supress_internal_errors=True)

		# If no record found:
		if not 'url' in record:
			if self.duration > 0:
				raise Warning(
					_('Unknown error - can`t retrieve the record!'))
			else:
				raise Warning(
					_('Calls with zero duration have no records!'))

		# Returns record player action:
		return {
			'type': 'ir.actions.act_url', 
			'url': record['url'], 
			'nodestroy': True, 
			'target': 'self'
		}

		# Javascript realization with HTML5 player, NYI:
		'''return {
			'type': 'ir.actions.client',
   			'tag': 'simbioz_binotel.download_record',
   			'params': {
   				'url': record['url']
   			}
		}'''


	# Making a call:
	@api.multi
	def make_call(self):
		return make_call(self.binotel_client_id)


# Binotel Operators:
class BinotelOperator(models.Model):
	_name = 'binotel.operator'
	client_ids = fields.One2many('binotel.client', 'operator_id')
	employee_id = fields.Integer(readonly=True)
	user_id = fields.Many2one('res.users', string=_('Responsible'))
	email = fields.Char(readonly=True)
	name = fields.Char(readonly=True)
	line = fields.Integer(readonly=True)
	status = fields.Selection([
		('online', 'Online'),
		('offline', 'Offline'),
		('inuse', 'In-Use'),
		('ringing', 'Ringing'),
	], readonly=True)


# Pushing new call information:
class BinotelController(http.Controller):

	# Envrionment:
	def get_env(self):
		cr, uid, context = request.cr, SUPERUSER_ID, request.context
		return api.Environment(cr, 1, {})

	def get_api(self, env):
		key_public, key_secret = BinotelSecure.get_keys(env.cr)
		return BinotelAPI(key_public, key_secret)


	# Call event handler:
	@http.route('/binotel/push', type='http', auth='none',
		methods=['GET', 'POST'], csrf=False)
	def update_calls(self, **post):

		# Skipping glitch:
		if 'requestType' in post:

			# Preparing API:
			env = self.get_env()
			binotel = self.get_api(env)
			key_public, key_secret = BinotelSecure.get_keys(env.cr)

			# Updating calls only if call is done:
			BinotelSettings.update_calls(key_public, key_secret, env, {
				'url':    'stats/call-details',
				'params': {'generalCallID': [post['generalCallID']]}
			})

		# Done:
		return BinotelAPI.dump({'status':'success'})


	# Call information event handler:
	@http.route('/binotel/crmlink', type='http', auth='none',
		methods=['GET', 'POST'], csrf=False)
	def update_url(self, **post):

		# Skipping glitch:
		if 'requestType' in post:

			# Customer data:
			if post['requestType'] == 'gettingCallSettings':

				# Preparing API:
				env = self.get_env()
				base_url = env['ir.config_parameter'].get_param('web.base.url')

				# Searching for a client:
				ids = env['binotel.client'].search_read(
					[('phone', '=', post['srcNumber'])])

				# Making new client link:
				if not ids:
					odoo_url = ''.join([base_url, 
						'/web#page=0&limit=80&view_type=list',
						'&model=binotel.client&menu_id=156&action=166'])
					string = 'Неизвестный клиент - перейти в Odoo'
					return BinotelAPI.dump({
						'customerData': {
							'linkToCrmUrl':   odoo_url,
							'linkToCrmTitle': string
						}
					})

				# Using existing client:
				else:
					client = ids[0]
					client_url = ''.join([
						base_url, '/web#id=', str(client['id']),
						'&view_type=form&model=binotel.client'
					])
					return BinotelAPI.dump({
						'customerData': {
							'name':           client['name'],
							'description':    client['description'],
							'linkToCrmUrl':   client_url,
							'linkToCrmTitle': 'Посмотреть в Odoo'
						}
					})

		# Done:
		return BinotelAPI.dump({'status':'success'})
