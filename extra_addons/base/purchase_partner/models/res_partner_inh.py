# -*- coding: utf-8 -*-
from odoo import api, fields, models


class ResPartnerCustom(models.Model):
    _inherit = 'res.partner'

    sklad_n = fields.Integer(string="Номер склада")
