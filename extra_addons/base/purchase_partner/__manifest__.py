{
    'name': 'Purchase partner',
    'version': '11.0.7.0.0',
    'category': 'Tools',
    'license': 'AGPL-3',
    'author': 'Eugene Golosuev, Simbioz',
    'website': 'http://www.simbioz.ua',
    'depends': ['base',
                'purchase',],
    'data': ['views/purchase_partner.xml',],
    'installable': True,
    'application': True,
    'sequence': 1,
}