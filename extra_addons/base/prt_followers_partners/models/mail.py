from odoo import models, fields, api


###############
# Mail.Thread #
###############
class PRTMailThread(models.AbstractModel):
    _name = "mail.thread"
    _inherit = "mail.thread"

    """
     Check subscribers
     """
    @api.multi
    def message_subscribe(self, partner_ids=None, channel_ids=None, subtype_ids=None, force=True):

        # Check whom to subscribe
        subscribe_option = self._context.get('subscribe_option', False)

        # Everyone
        if not subscribe_option or subscribe_option == '0':
            return super(PRTMailThread, self).message_subscribe(partner_ids=partner_ids, channel_ids=channel_ids,
                                                                subtype_ids=subtype_ids, force=force)
        # Only me
        if subscribe_option == '8':
            return super(PRTMailThread, self).message_subscribe(partner_ids=[self.env.user.partner_id.id], channel_ids=channel_ids,
                                                                subtype_ids=subtype_ids, force=force)
        # No one
        if subscribe_option == '9':
            return True

        # Some users
        new_ids = []
        partners = self.env['res.partner'].sudo().browse(partner_ids)

        # Scan partners
        for partner in partners:
            users = partner.user_ids
            if not users or len(users) == 0:
                continue

            # Remove users that are not Employees if such and option was selected
            if subscribe_option in ['3', '4']:
                for user in users:
                    if not user.has_group('base.group_user'):
                        users -= user
                if len(users) == 0:
                    continue

            # Users only
            if subscribe_option in ['1', '3']:
                new_ids.append(partner.id)

            # Users of same company
            elif subscribe_option in ['2', '4']:
                same_company = True
                our_company_id = self.env.user.company_id.id
                for company in users.mapped('company_id'):
                    if company.id != our_company_id:
                        same_company = False
                        break
                if same_company:
                    new_ids.append(partner.id)

        return super(PRTMailThread, self).message_subscribe(partner_ids=new_ids, channel_ids=channel_ids,
                                                            subtype_ids=subtype_ids, force=force)


########################
# Mail.Compose Message #
########################
class PRTMailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'
    _name = 'mail.compose.message'

    subscribe_option = fields.Selection([
        ('0', 'Everyone'),
        ('1', 'Users only'),
        ('2', 'Users of current company only'),
        ('3', 'Employees only'),
        ('4', 'Employees of current company only'),
        ('8', 'Only me'),
        ('9', 'No one'),
    ], string="Auto subscribe", required=True,
        default='0',
        help="Choose partners to be subscribed to this thread")

# -- Send mail
    @api.multi
    def send_mail(self, auto_commit=False):
        self = self.with_context(subscribe_option=self.subscribe_option)
        super(PRTMailComposer, self).send_mail(auto_commit=auto_commit)
