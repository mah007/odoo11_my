# -*- coding: utf-8 -*-
{
    'name': 'Auto Subscribe Followers Selection: Select Partners to add as Followers in mail composer to avoid unwanted messages',
    'version': '11.0.1.0',
    'author': 'Ivan Sokolov',
    'category': 'Productivity',
    'license': 'GPL-3',
    'website': 'https://demo.promintek.com',
    'description': """
    Select which partners to add as followers in mail message wizard               
""",
    'depends': ['base', 'mail'],
    'images': ['static/description/banner.png'],
    'data': [
        'views/prt_mail.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False
}
