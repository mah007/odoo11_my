function openerp_pos_printout_improvement(instance, module){
    var QWeb = instance.web.qweb;
    var _t = instance.web._t;
    var orderline_id = 1;
    var test = 0;

    module.Order = module.Order.extend( {
        getTotalQuantity: function() {
            return (this.get('orderLines')).reduce((function(sum, orderLine) {
                return sum + orderLine.get_quantity();
            }), 0);
        },
    });

    instance.point_of_sale.ReceiptScreenWidget.include({
        print: function(){
            var self = this;
            var $barcode_img = self.$el.find('.barcode_img') ;
            if( $barcode_img.length == 0  ){
                self._super.apply(self, arguments);
            }else{
                $barcode_img[0].onload = function(e){ window.print(); };
            }
        },
    });
}