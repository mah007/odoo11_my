{
    'name': 'POS print',
    'version': '8.0.2',
    'category': 'POS',
    'depends': [
        'point_of_sale',
    ],
    'author': 'Leonid Kolesnichenko',
    'license': 'AGPL-3',
    'data': [
        #'views/js.xml',
    ],
    'qweb': [
        'static/src/xml/printout.xml',
    ],
    'installable': True,
    'application': False,
}
