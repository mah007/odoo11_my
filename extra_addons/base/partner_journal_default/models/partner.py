# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Partner(models.Model):
    _inherit = 'res.partner'

    sale_journal = fields.Many2one('account.journal', 'Default journal', domain=[('type', '=', 'sale')])
    purchase_journal = fields.Many2one('account.journal', 'Default journal', domain=[('type', '=', 'purchase')])

class Invoice(models.Model):
    _inherit = 'account.invoice'

    @api.model
    def create(self, vals):
      if self.partner_id.sale_journal and self.type == 'out_invoice':
        vals['journal_id'] = self.partner_id.sale_journal.id
      if self.partner_id.purchase_journal and self.type == 'in_invoice':
        vals['journal_id'] = self.partner_id.purchase_journal.id
      invoice = super(Invoice, self).create(vals)
      return invoice

    @api.onchange('partner_id')
    def partner_onchange(self):
      if self.partner_id.sale_journal and self.type == 'out_invoice':
        self.journal_id = self.partner_id.sale_journal.id
      if self.partner_id.purchase_journal and self.type == 'in_invoice':
        self.journal_id = self.partner_id.purchase_journal.id

