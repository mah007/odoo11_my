# -*- coding: utf-8 -*-

from odoo import models, fields


class Tax(models.Model):
    _inherit = 'account.tax'

    class_code = fields.Many2one('account.tax.class', 'Tax Class')

class TaxClass(models.Model):
    _name = 'account.tax.class'
    _rec_name = 'code'

    code = fields.Char('Code' , required=True, size=8)
    description = fields.Char('Description' , required=True, size=256)
    non_standard = fields.Boolean('Non-standard Tax')

    _sql_constraints = [
        ('code_uniq', 'unique (code)',
         'The code of the Tax must be unique !')
    ]

