# -*- coding: utf-8 -*-
{
    'name': 'Tax Classification Codes',
    'images': ['images/main_screenshot.png'],
    'version': '1.0.1',
    'category': 'Tax',
    'summary': 'Adds tax code according to the VAT classification',
    'author': 'Inovacijos',
    'website': 'http://www.innovations.lt',
    'depends': [
        'account',
    ],
    'installable': True,
    'license': 'AGPL-3',
    'data': [
        'views/account_tax_class.xml',
    ],
}
