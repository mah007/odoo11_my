# -*- coding: utf-8 -*-
##############################################################################
#
#    This module uses OpenERP, Open Source Management Solution Framework.
#    Copyright (C) 2017-Today Sitaram
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from odoo import api, fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    inv_amount = fields.Float('Invoice Amount', compute='_compute_invoice_values')
    inv_due_amount = fields.Float('Invoice Due Amount', compute='_compute_invoice_values')
    inv_paid_amount = fields.Float('Invoice Paid Amount', compute='_compute_invoice_values')

    @api.multi
    def _compute_invoice_values(self):
        total = due = paid = 0
        for a in self.invoice_ids:
            total += a.amount_total
            if a.state != 'draft':
                due += a.residual
                paid += a.amount_total - a.residual
            else:
                due += a.amount_total
        self.inv_amount = total
        self.inv_due_amount = due
        self.inv_paid_amount = paid
