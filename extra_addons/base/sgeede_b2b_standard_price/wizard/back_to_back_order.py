# -*- coding: utf-8 -*-
import logging

from odoo import fields, models, _
from odoo import SUPERUSER_ID, api

_logger = logging.getLogger(__name__)


class back_to_back_order(models.TransientModel):
	_inherit = 'back.to.back.order'

	@api.model
	def default_get(self, fields):
		context = dict(self._context or {})
		res = super(back_to_back_order, self).default_get(fields)
		order = self.env['sale.order'].browse(self._context.get('active_ids'))
		items = []
		for line in order.order_line:
			item = 0, 0, {
				'product_id': line.product_id.id,
				'qty': line.product_uom_qty,
				'price': line.product_id.standard_price,
				'subtotal':line.price_subtotal
			}
			if line.product_id:
				items.append(item)
		res.update(line_ids=items)
		return res

