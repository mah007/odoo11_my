# -*- coding: utf-8 -*-
{
    'name': 'sgeede_b2b_standard_price',
    'summary': '''Change Price field in Purchase Order into standard price''',

    'author': 'Simbioz',
    'website': 'http://www.simbioz.ua',
    'category': 'Tools',
    'version': '11.0.0.1',
    'depends': ['base','sale','purchase','sgeede_b2b'],
    'data': [
    ],
    'demo': [],
    'installable': True,
}
