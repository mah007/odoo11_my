from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import UserError, AccessError
import logging

_logger = logging.getLogger(__name__)


class SaleOrderEg(models.Model):
    _inherit = "sale.order"

    s_courier_id = fields.Many2one('hr.employee', string="Courier")