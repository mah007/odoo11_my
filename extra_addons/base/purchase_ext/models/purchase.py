from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import UserError, AccessError


import logging

_logger = logging.getLogger(__name__)


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    @api.one
    def _number_sklad(self):
        if self.partner_id:
            if self.partner_id.sklad_n:
                self.n_sklad = self.partner_id.sklad_n

    courier_id = fields.Many2one('hr.employee', string="Courier")
    priority_track = fields.Char(string="Приоритет маршрута")
    state = fields.Selection([
        ('draft', 'RFQ'),
        ('dostavka', 'Доставка'),
        ('sent', 'RFQ Sent'),
        ('to approve', 'To Approve'),
        ('purchase', 'Purchase Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled')
        ], string='Status', readonly=True, index=True, copy=False, default='draft', track_visibility='onchange')
    n_sklad = fields.Integer(string="Номер склада", compute=_number_sklad)

    @api.multi
    def button_confirm(self):
        for order in self:
            if order.state not in ['draft', 'dostavka', 'sent']:
                continue
            order._add_supplier_to_product()
            # Deal with double validation process
            if order.company_id.po_double_validation == 'one_step'\
                    or (order.company_id.po_double_validation == 'two_step'\
                        and order.amount_total < self.env.user.company_id.currency_id.compute(order.company_id.po_double_validation_amount, order.currency_id))\
                    or order.user_has_groups('purchase.group_purchase_manager'):
                order.button_approve()
            else:
                order.write({'state': 'to approve'})

            stock = self.env['stock.picking'].search([('group_id', '=', order.name)])

            if stock:
                if order.courier_id:
                    stock.write({'e_carrier_id': order.courier_id.id})

        return True

    @api.multi
    def action_rfq_send(self):
        '''
        This function opens a window to compose an email, with the edi purchase template message loaded by default
        '''
        self.ensure_one()
        ir_model_data = self.env['ir.model.data']
        try:
            if self.env.context.get('send_rfq', False):
                template_id = ir_model_data.get_object_reference('purchase', 'email_template_edi_purchase')[1]
            else:
                template_id = ir_model_data.get_object_reference('purchase', 'email_template_edi_purchase_done')[1]
        except ValueError:
            template_id = False
        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        ctx = dict(self.env.context or {})
        ctx.update({
            'default_model': 'purchase.order',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'custom_layout': "purchase.mail_template_data_notification_email_purchase_order",
            'force_email': True
        })

        if self.state == 'dostavka':
            self.state = 'sent'

        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }

    @api.multi
    def button_confirm(self):
        for order in self:
            if order.state not in ['draft', 'dostavka', 'sent']:
                continue
            order._add_supplier_to_product()
            # Deal with double validation process
            if order.company_id.po_double_validation == 'one_step'\
                    or (order.company_id.po_double_validation == 'two_step'\
                        and order.amount_total < self.env.user.company_id.currency_id.compute(order.company_id.po_double_validation_amount, order.currency_id))\
                    or order.user_has_groups('purchase.group_purchase_manager'):
                order.button_approve()

            if order.state == 'dostavka':
                order.write({'state': 'sent'})

            else:
                order.write({'state': 'to approve'})
        return True

    @api.one
    def state_dostavka(self):
        self.state = 'dostavka'

    @api.multi
    def action_form_view_purchase_ext(self):
        context = self.env.context.copy()
        context['view_buttons'] = True
        view = {
            'name': _('Details'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'purchase.order',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'res_id': self.id,
            'context': context
        }
        return view

    @api.model
    def create(self,values):
        res = super(PurchaseOrder,self).create(values)

        if res.order_line:
            for order in res.order_line:
                if res.courier_id:
                    order.write({'l_courier_id': res.courier_id.id})

                if res.priority_track:
                    order.write({'l_priority_track': res.priority_track})

        return res

    @api.multi
    def write(self, vals):
        res = super(PurchaseOrder, self).write(vals)

        if self.order_line:
            for order in self.order_line:
                if self.courier_id:
                    order.write({'l_courier_id': self.courier_id.id})

                if self.priority_track:
                    order.write({'l_priority_track': self.priority_track})

        return res


class PurOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    l_courier_id = fields.Many2one('hr.employee', string="Courier")
    l_priority_track = fields.Char(string="Приоритет маршрута")



