from odoo import api, fields, models


import logging

_logger = logging.getLogger(__name__)


class StockPicking(models.Model):
    _inherit = "stock.picking"

    e_carrier_id = fields.Many2one('hr.employee', string="Courier")