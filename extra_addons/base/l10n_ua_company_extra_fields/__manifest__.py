# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Ukraine - Company extra fields',
    'author': 'ERP Ukraine',
    'website': 'https://erp.co.ua',
    'version': '1.0',
    'description': """
Ukrainian Localization.
=======================

Add extra fields on company form:

    * Company Registry
    * License Number

    """,
    'category': 'Localization',
    'depends': ['l10n_ua'],
    'data': [
        'views/company_view.xml',
        'views/partner_view.xml',
    ],
    'auto_install': False,
}
