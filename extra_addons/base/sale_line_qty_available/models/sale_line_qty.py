# -*- coding: utf-8 -*-

from odoo import models, fields


class SaleOrderLineQty(models.Model):
    _inherit = 'sale.order.line'

    sale_line_qty_available = fields.Float(related="product_id.qty_available", string="Quantity on hand")
