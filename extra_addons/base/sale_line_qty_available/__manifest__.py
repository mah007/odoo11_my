# -*- coding: utf-8 -*-

{
    'name': 'Sale Line Quantity Available',
    'summary': 'Show quantity available in order line',
    'category': 'Sale',
    'version': '11.0.0.1',
    'author': 'Nikita Simbioz',
    'website': 'https://www.simbioz.ua',
    'depends': [
        'sale',
        'stock',
    ],
    'data': [
        'views/sale_view.xml'
    ],
    'installable': True,
    'application': True,
    'sequence': 1,
}