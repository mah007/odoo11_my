odoo.define('hide_menu.navbar', function (require) {
    'use strict';

    const core = require('web.core');
    const webMenu = require('web.Menu');
    const QWeb = core.qweb;
    const rpc = require('web.rpc');
    

    webMenu.include({
        start(){
            let res = this._super.apply(this, arguments);
            const $btn_hide_menu = this.$el.parent().
                prepend( $(`
                <div class="btn_toggle__o_sub_menu" title="Menu Expand">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </div>`) ).
                find('.btn_toggle__o_sub_menu');

            this.getManuState() == '1' ? this.$secondary_menus.addClass('o_sub_menu__hidden') : null;

            $btn_hide_menu.on('click', ()=>{
                this.toggleMenuState(this.$secondary_menus);
            });

            const config = { attributes: true};


            this.current_margin_left = $('.o_main_content')[0].style.marginLeft;

            const callback = (mutationsList) => {
                if(  this.$secondary_menus.first().hasClass('o_hidden')){
                    $('.o_main_content')[0].style.marginLeft = 0;
                }else if( this.getManuState() == '1' ){
                    $('.o_main_content')[0].style.marginLeft = this.current_margin_left;
                }
            };

            let observer = new MutationObserver(callback);

            observer.observe(this.$secondary_menus[0], config);

            return res;
        },
        toggleMenuState($el_sub_men){
            if( this.getManuState() == '1' ){
                $('.o_main_content')[0].style.marginLeft = 0;
                $el_sub_men.addClass('position_relative'); 
                $el_sub_men.removeClass('o_sub_menu__hidden'); 
                this.setManuState('0');
                this.current_margin_left = $('.o_main_content')[0].style.marginLeft; 
            }else{
                $el_sub_men.addClass('o_sub_menu__hidden');

                setTimeout( ()=>{
                    if( this.getManuState() == '1' ){
                        $el_sub_men.removeClass('position_relative'); 
                        $('.o_main_content')[0].style.marginLeft = '25px';
                        this.current_margin_left = $('.o_main_content')[0].style.marginLeft; 
                    }
                }, 700);

                this.setManuState('1'); 
            }
        },
        getManuState(){
            const menu = 'menuState'
            const matches = document.cookie.match(new RegExp(
                "(?:^|; )" + menu.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        },
        setManuState( state ){
            document.cookie = `menuState=${state}`;
        }
    })
});