# -*- coding: utf-8 -*-

from odoo import fields, models, api, _


class HideMenuUserConfig(models.Model):
    _inherit = 'res.users'

    left_menu_stage = fields.Boolean(string="Left menu stage", default=False)

    @api.model
    def get_menu_stage(self):
        return self.env['res.users'].browse(self._uid)['left_menu_stage']
   
    @api.model
    def toggle_menu_stage(self):
        user_record = self.env['res.users'].browse(self._uid)
        user_record['left_menu_stage'] = not  user_record['left_menu_stage'] 
        return user_record['left_menu_stage'] 
