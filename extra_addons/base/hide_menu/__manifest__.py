# -*- coding: utf-8 -*-

{
    'name': 'Toggle hide menu',
    'version': '11.0.0.0.1',
    'author': 'Vitaliy Demchenko',
    'website': 'https://github.com/forij',
    'depends': ['web'],
    'data': [
        'views/link.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
