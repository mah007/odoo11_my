# -*- coding: utf-8 ---*---
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name' : 'Sales Order Weight',
    'version' : '1.0',
    'summary': 'Total weight of an order',
    'sequence': 30,
    'license':'LGPL-3',
    'description': """
Calculates total weight of a sale order, which is the sum of individual weights of each unit of the products in the order
    """,
    'category': 'Generic Modules/Sales',
    'author' : 'BroadTech IT Solutions Pvt Ltd',
    'website' : 'http://www.broadtech-innovations.com',
    'images': ['static/description/banner.jpg'],
    'depends' : ['sale_management'],
    'data': [
        'views/sale_weight_view.xml',
    ],
    'demo': [
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
