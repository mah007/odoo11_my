# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Ukraine - User Facsimile',
    'author': 'ERP Ukraine',
    'website': 'https://erp.co.ua',
    'version': '1.0',
    'description': """
Ukrainian Localization.
=======================

Add Facsimile field on user form.

It can be used on pdf reports.
    """,
    'category': 'Localization',
    'depends': ['l10n_ua'],
    'data': [
        'views/res_users_view.xml',
    ],
    'auto_install': False,
}
