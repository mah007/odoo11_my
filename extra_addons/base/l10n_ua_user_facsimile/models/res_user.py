from odoo import fields, models


class UsersFacsimile(models.Model):
    _inherit = "res.users"

    facsimile = fields.Binary(
        'Facsimile',
        help="Upload you signature to show it on pdf reports.")
