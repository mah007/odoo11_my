# -*- coding: utf-8 -*-
from openerp import api, fields, models


class BlogPostCustomFields(models.Model):
    _inherit = "blog.post"

    @api.multi
    def open_blog_custom_field_form(self):
        my_blog_model = self.env['ir.model'].search([('model', '=', 'blog.post')])[0]
        custom_blog_model_view = self.env['ir.model.data'].sudo().get_object('blog_custom_fields',
                                                                             'ir_model_view_form_custom_blog_fields')

        return {'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'ir.model',
                'res_id': my_blog_model.id,
                'view_id': custom_blog_model_view.id,
                'target': 'new', }
