# -*- coding: utf-8 -*-
from openerp import api, fields, models
import logging
_logger = logging.getLogger(__name__)


class IrModelEBlogFieldss(models.Model):

    _inherit = "ir.model"


    e_blog_custom_field_ids = fields.One2many('ir.model.fields',
                                              'e_blog_custom_model_id',
                                              string="Custom Fields",
                                              domain=[('e_blog_custom_field','=',True)])

    @api.one
    def fake_save2(self):
        """Function does nothing, save gets called after"""
        pass

    @api.multi
    def write(self, values):

        ins = super(IrModelEBlogFieldss, self).write(values)

        e_blog_custom_form_fields = self.env['ir.model.data'].sudo().get_object('blog_custom_fields',
                                                                              'view_form_inherit_blog_custom_fields')

        e_blog_custom_form_fields_string = ""

        e_blog_custom_form_fields_string += "<sheet position=\"inside\">\n"
        e_blog_custom_form_fields_string += "    <notebook>\n"
        e_blog_custom_form_fields_string += "        <page string=\"Custom Fields\">\n"
        e_blog_custom_form_fields_string += "            <group>\n"

        for e_blog_custom_f in self.env['ir.model.fields'].sudo().search([('e_blog_custom_field', '=', True)]):
            e_blog_custom_form_fields_string += "                <field name=\"" + str(e_blog_custom_f.name) + "\""

            if e_blog_custom_f.e_blog_custom_field_widget:
                e_blog_custom_form_fields_string += " widget=\"" + e_blog_custom_f.e_blog_custom_field_widget.internal_name + "\""

            e_blog_custom_form_fields_string += "/>"

        e_blog_custom_form_fields_string += "                <button name=\"open_blog_custom_field_form\" type=\"object\" groups=\"base.group_system\" string=\"Add Custom Field\"/>\n"
        e_blog_custom_form_fields_string += "            </group>\n"
        e_blog_custom_form_fields_string += "        </page>\n"
        e_blog_custom_form_fields_string += "    </notebook>"
        e_blog_custom_form_fields_string += "</sheet>"

        e_blog_custom_form_fields.arch = e_blog_custom_form_fields_string

        return ins