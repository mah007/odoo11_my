# -*- coding: utf-8 -*-
import logging
from openerp import api, fields, models

_logger = logging.getLogger(__name__)


class IrModelEBlogFields(models.Model):

    _inherit = "ir.model.fields"

    e_blog_custom_model_id = fields.Many2one('ir.model', string="Custom Model")
    e_blog_limited_types = fields.Selection([('char','Single Line Textbox'),
                                             ('text','Multi Line Textbox'),
                                             ('date','Date'),
                                             ('datetime','Date Time'),
                                             ('selection', 'Static Dropdown')],
                                            default="char",
                                            string="Field Type")
    e_blog_custom_name = fields.Char(string="Field Name")
    e_blog_custom_field = fields.Boolean(string="Custom BLOG Field")
    e_blog_custom_field_widget = fields.Many2one('crm.custom.fields.widget', string="Widget")
    e_blog_custom_field_selection_ids = fields.One2many('ir.model.fields.selections', 'imf_id', string="Selection Options")
    
    @api.onchange('e_blog_limited_types')
    def _onchange_blog_limited_types(self):
        self.ttype = self.e_blog_limited_types
        
    @api.onchange('e_blog_custom_field_selection_ids')
    def _onchange_e_byte_custom_field_selection_ids(self):
        sel_string = ""
        for sel in self.e_blog_custom_field_selection_ids:
            sel_string += "('" + sel.internal_name + "','" + sel.name + "'), "
            
        self.selection = "[" + sel_string[:-2] + "]"
        
    @api.model
    def create(self, values):
        """Assign name when it's actually saved overwise they all get the same ID"""
        
        if 'e_blog_custom_field' in values:
            temp_name = "x_custom_" + values['field_description']
            temp_name = temp_name.replace(" ","_").lower()
            values['name'] = temp_name
            values['model_id'] = self.env['ir.model.data'].get_object('website_blog','model_blog_post').id
            _logger.error(temp_name)

        return super(IrModelEBlogFields, self).create(values)
        
class EBlogCustomFieldsWidget(models.Model):

    _name = "crm.custom.fields.widget"

    name = fields.Char(string="Name")
    internal_name = fields.Char(string="Internal Name", help="The technicial name of the widget")

class IrModelEBlogFieldsSelection(models.Model):

    _name = "ir.model.fields.selections"
    
    imf_id = fields.Many2one('ir.model.fields', string="Field")
    name = fields.Char(string="Name", required="True")
    internal_name = fields.Char(string="Internal Name", required="True")