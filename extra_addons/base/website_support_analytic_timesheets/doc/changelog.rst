v1.0.2
======
* Fix email Jinja for loop getting messed up inside table

v1.0.1
======
* Filter out closed email template from compose window
* Remove timesheet table if timesheets is empty (if people bypass the close button...)

v1.0
====
* Port to version 11