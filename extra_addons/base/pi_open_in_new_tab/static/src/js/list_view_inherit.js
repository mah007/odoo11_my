openerp.pi_open_in_new_tab = function(openerp) {
"use strict";

var self;

var tab_limit = 8;  // Tab limits
/**  open new tab **/
openerp.web.ListView.List.include({
    init: function (group, opts) {
           this._super.apply(this, arguments);
        var self = this;
        self.action_manager = new openerp.web.ActionManager(self);
        this.$current = $('<tbody>')
            .delegate('input[readonly=readonly]', 'click', function (e) {

                e.preventDefault();
            })
            .delegate('th.oe_list_record_selector', 'click', function (e) {
                e.stopPropagation();
                var selection = self.get_selection();
                var checked = $(e.currentTarget).find('input').prop('checked');
                $(self).trigger(
                        'selected', [selection.ids, selection.records, ! checked]);
            })
            .delegate('td.oe_list_record_delete button', 'click', function (e) {
                e.stopPropagation();
                var $row = $(e.target).closest('tr');
                $(self).trigger('deleted', [[self.row_id($row)]]);
            })
            .delegate('td.oe_list_field_cell button', 'click', function (e) {
                e.stopPropagation();
                var $target = $(e.currentTarget),
                      field = $target.closest('td').data('field'),
                       $row = $target.closest('tr'),
                  record_id = self.row_id($row);

                if ($target.attr('disabled')) {
                    return;
                }
                $target.attr('disabled', 'disabled');

                $(self).trigger('action', [field.toString(), record_id, function (id) {
                    $target.removeAttr('disabled');
                    return self.reload_record(self.records.get(id));
                }]);
            })
            .delegate('a', 'click', function (e) {
                e.stopPropagation();
            })
            .delegate('tr', 'click', function (e) {
                var row_id = self.row_id(e.currentTarget);
                if (row_id) {
                    e.stopPropagation();
                    if (!self.dataset.select_id(row_id)) {
                        throw new Error(_t("Could not find id in dataset"));
                    }
                    self.row_clicked(e);
                }
            })
            /* Pierce */
            .delegate('#new_tab', 'click', function (e) {
                e.stopPropagation();
                var parent = $(this).parent();
               var record_id = $(this).parent().attr("data-id");
               var row_id = self.row_id($(this).parent());
               var model = self.dataset.model;
                var context = self.dataset.context;
                 var contexts = context.__contexts;
                 var action_id ;
                var menu_id;
                 var model_name;
                 if(contexts){
                for (var contextVar in contexts){
                    if(contexts[contextVar].params){
                         menu_id = contexts[contextVar].params.menu_id;
                action_id = contexts[contextVar].params.action;
                console.log("action:"+action_id);
                if(!model_name){
            model_name = contexts[contextVar].params.model;
            }
        }
    }
}
            var getUrl = window.location;
            var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
                self.action_manager = new openerp.web.ActionManager(self);
                self.action_manager.do_action({
                    type: 'ir.actions.act_url',
                    url:baseUrl+'#id='+record_id+'&view_type=form&model='+model+'&action='+action_id,
                    target: 'new'
                });

            })

    },
});

/** Open Multiple tabs **/
	var _t = openerp.web._t;
	var _lt = openerp.web._lt;
	  var QWeb = openerp.web.qweb;

openerp.web.Sidebar.include({
    init: function () {
        var self = this;
        this._super.apply(this, arguments);
        self.sections.push({
            name: 'open_in_new_tab',
            label: _t('Open In New Tab')
        });
        self.items['open_in_new_tab'] =  [];
        var view = self.getParent();
        if (view.fields_view && view.fields_view.type === "tree") {
            self.open_tab();
        }
    },

    open_tab: function () {
        var browser = true;
           var self = this;
            self.add_items('open_in_new_tab', [{
            label: 'Open',
            callback: self.open_new_tab,
          },]);
    },

    open_new_tab: function () {
        var ids = Array();
        var link_array = Array();
        var self = this,
        view = this.getParent(),
        children = view.getChildren();
        var params_obj = view.dataset.context.params;
        for(var actions in params_obj ){
            var action_id = params_obj.action;
            var menu_id = params_obj.menu_id;
            }
        if (children) {
            children.every(function (child) {
                if (child.field && child.field.type == 'one2many') {
                    view = child.viewmanager.views.list.controller;
                    return false;
                }
                if (child.field && child.field.type == 'many2many') {
                    view = child.list_view;
                    return false;
                }
                return true;
            });
        }

        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        var model=view.model;
        var rows = view.$el.find('tbody tr[data-id]');
        var tab_arr = "";
        $.each(rows, function () {
            var $row = $(this);
            var row_selector = '.o_list_record_selector input[type=checkbox],\
            .oe_list_record_selector input[type=checkbox]';
            var checked = $row.find(row_selector).is(':checked');
            if (children && checked === true) {
                 var record_id =  $row.attr("data-id");
                 ids.push($row.attr("data-id"));
                 var url = baseUrl+'#id='+$row.attr("data-id")+'&view_type=form&model='+model+'&action='+action_id+'&menu_id='+menu_id;
                 link_array.push(url);
                 tab_arr += "window.open('" + url +"', '_blank');";

            }
        });
        if(link_array.length <= tab_limit){    // check tabes length more than 8
            eval(tab_arr);
        }else{
             this.do_warn(_t("Maximum 8 Tabs are allowed"), true);
         }
    },
});
};