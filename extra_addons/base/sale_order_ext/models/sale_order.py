from odoo import api, fields, models, _

import logging
_logger = logging.getLogger(__name__)


class SaleOrderInh(models.Model):
    _inherit = "sale.order"

    @api.multi
    def open_sale_orders(self):
        context = self.env.context.copy()
        context['view_buttons'] = True
        view = {
            'name': _('Sale Orders'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'sale.order',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('partner_id', '=', self.partner_id.id), ('state', '=', 'sale')],
            'context': context
        }
        return view

    @api.multi
    def open_credit_notes(self):
        context = self.env.context.copy()
        context['view_buttons'] = True
        context['default_type'] = 'out_refund'
        context['type'] = 'out_refund'
        context['journal_type'] = 'sale'
        view = {
            'name': _('Credit Notes'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'account.invoice',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('state', 'in', ['draft', 'paid']), ('type', '=', 'out_refund'), ('partner_id', '=', self.partner_id.id)],
            'context': context
        }
        return view



    @api.one
    def _get_total_summary_sale_orders(self):
        sale_orders = self.env['sale.order'].search(
            [('partner_id', '=', self.partner_id.id), ('state', '=', 'sale')])
        summmary = 0.0
        for sale in sale_orders:
            summmary += sale.amount_total
        self.sale_summ = summmary

    @api.one
    def _get_total_summary_credit_notes(self):
        credit_notes = self.env['account.invoice'].search([('state', 'in', [
                                                          'draft', 'paid']), ('type', '=', 'out_refund'), ('partner_id', '=', self.partner_id.id)])
        summmary = 0.0
        for note in credit_notes:
            summmary += note.amount_total
        self.kredit_note_summ = summmary

    sale_summ = fields.Float('Total Summary Saleorders',
                             compute=_get_total_summary_sale_orders)
    kredit_note_summ = fields.Float(
        'Total Summary Credit Notes', compute=_get_total_summary_credit_notes)

    # --------------------------

    @api.multi
    def open_sale_order_line(self):
        context = self.env.context.copy()
        context['view_buttons'] = True
        view = {
            'name': _('Sale Order Lines'),
            'view_type': 'form',
            'view_mode': 'tree',
            'res_model': 'sale.order.line',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'domain': [('order_partner_id', '=', self.partner_id.id), ('state', '=', 'sale')],
            'context': context
        }
        return view


    @api.multi
    def _get_sale_order_line(self):
        for record in self:
            sale_orders = self.env['sale.order'].search(
                [('partner_id', '=', record.partner_id.id), ('state', '=', 'sale')])
            summmary2 = 0.0
            for sale in sale_orders:
                summmary2 += sale.amount_total
            record.sale_summ2 = summmary2

    sale_summ2 = fields.Float('Total Saleorder Lines',
                              compute=_get_sale_order_line)




class SaleOrderLineInh(models.Model):
    _inherit = "sale.order.line"

    comment = fields.Text("Comment")

    @api.multi
    def open_product(self):
        context = self.env.context.copy()
        context['view_buttons'] = True
        view = {
            'name': _('Product details'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'product.product',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.product_id.id,
            'context': context
        }
        return view


