from odoo import api, fields, models, _

import logging
_logger = logging.getLogger(__name__)

class SupplierInfoInh(models.Model):
    _inherit = "product.supplierinfo"

    @api.multi
    def _comment_d(self):
        for rec in self:
            if rec.name:
                if rec.name.comment:
                    rec.p_comment = rec.name.comment

    phone = fields.Char('Phone', related="name.phone")
    p_comment = fields.Text(string="Comment", compute=_comment_d)
    my_sklad = fields.Integer('Warehouse', related="name.sklad_n")


