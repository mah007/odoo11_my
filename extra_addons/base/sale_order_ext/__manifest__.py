# -*- coding: utf-8 -*-
{
    'name': 'Sale order ext',
    'version': '11.0.4.0',
    'category': 'Sale',
    'license': 'AGPL-3',
    'author': 'Leonid Kolesnichenko, Eugene Golosuev, Mykyta Maistrenko Simbioz',
    'website': 'http://www.simbioz.ua',
    'depends': ['sale','purchase','product','purchase_partner',],
    'data': [
        'views/sale_order_view.xml',
        'views/product_view.xml'
        ],
    'installable': True,
    'application': True,
    'sequence': 1,
}
