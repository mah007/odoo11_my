{
    'name': 'Partner Reference mandatory',
    'images': ['images/main_screenshot.png'],
    'version': '1.0.2',
    'category': 'Partner',
    'summary': 'Makes Reference field mandatory and unique in the Partner form',
    'author': 'Inovacijos',
    'website': 'http://www.innovations.lt',
    'depends': [
    ],
    'installable': True,
    'license': 'AGPL-3',
    'data': [
    ],
}
