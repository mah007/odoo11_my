# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import Warning


class Partner(models.Model):
    _inherit = 'res.partner'

    ref = fields.Char(required=True)

    @api.model
    def create(self, vals):
      if not 'ref' in vals:
        partner = super(Partner, self).create(vals)
        return partner
      ref = self.env['res.partner'].search([('ref', '=', vals['ref'])])
      if ref:
        raise Warning('Internal Reference field must be unique!')
      partner = super(Partner, self).write(vals)
      return partner
   
    @api.multi
    def write(self, vals):
      if not 'ref' in vals:
        partner = super(Partner, self).write(vals)
        return partner
      ref = self.env['res.partner'].search([('ref', '=', vals['ref'])])
      if ref:
        raise Warning('Internal Reference field must be unique!')
      partner = super(Partner, self).write(vals)
      return partner
