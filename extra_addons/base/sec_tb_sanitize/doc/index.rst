===================
Sanitize Tracebacks
===================

Installation
------------

#. Unzip and copy the module directory to your Odoo addons directory.
#. Login to your Odoo instance as a user with administrative capabilities.
#. Enable debug mode (*Settings -> Activate debug mode*) or insert *&debug=1*
   query parameter in the URL (after the question mark *?* and before *#*).
#. Go to *Apps* menu, click *Update Apps List* and in the wizard window click
   the *Update* button.
#. Remove the default *Apps* filter in the *Apps* view and search for the
   *Sanitize Tracebacks* module.
#. Click the *Install* button on the *Sanitize Tracebacks* module record.

Uninstalling the module
-----------------------

#. Login to your Odoo instance as a user with administrative capabilities.
#. Go to *Apps* menu, remove the default *Apps* filter in the search field and
   search for the *Sanitize Tracebacks* module.
#. Select the result module and click on the *Uninstall* button.
#. Restart the *Odoo* server process.
