# -*- coding: utf-8 -*-
# Copyright 2017 Naglis Jonaitis
# License AGPL-3 or later (https://www.gnu.org/licenses/agpl).

import logging

from odoo import _, api, http, models, tools

from ..utils import sanitize_traceback

_logger = logging.getLogger(__name__)


def install_serialize_exception_wrapper():
    original_func = http.serialize_exception

    # Don't wrap the function repeatedly, eg. on module update.
    if getattr(original_func, 'sec_exc_sanitize_wrapped', False):
        return False

    def wrapper(e):
        result = original_func(e)
        tb = result.get('debug')
        if not tb:
            return result
        try:
            sanitized_tb = sanitize_traceback(tb)
        except Exception:
            # Something very unexpected happened (bug?).
            # Remove the traceback completely and inform the user.
            sanitized_tb = _(
                u'An unexpected exception occured while trying to '
                u'sanitize the exception traceback.')
            _logger.exception(sanitized_tb)
        result.update(debug=sanitized_tb)
        return result

    # Mark the function as wrapped.
    wrapper.sec_exc_sanitize_wrapped = True

    # Store the original function in case we want to restore it later.
    wrapper.original_serialize_exception = original_func

    http.serialize_exception = wrapper

    return True


def uninstall_serialize_exception_wrapper():
    wrapper = http.serialize_exception
    original_func = getattr(wrapper, 'original_serialize_exception', None)

    # The function was not wrapped.
    if original_func is None:
        return False

    # Restore the original function.
    http.serialize_exception = original_func

    return True


class IrHttp(models.AbstractModel):
    _inherit = 'ir.http'

    @api.model_cr
    def _register_hook(self):
        # Don't sanitize tracebacks if in dev mode.
        in_dev_mode = 'werkzeug' in tools.config['dev_mode']
        if not in_dev_mode and install_serialize_exception_wrapper():
            _logger.debug(u'Traceback sanitization wrapper installed')
        return super(IrHttp, self)._register_hook()
