# -*- coding: utf-8 -*-
# Copyright 2017 Naglis Jonaitis
# License AGPL-3 or later (https://www.gnu.org/licenses/agpl).
{
    'name': 'Sanitize Tracebacks',
    'version': '11.0.1.0.0',
    'author': 'Naglis Jonaitis',
    'category': 'Extra Tools',
    'website': 'https://github.com/naglis',
    'license': 'AGPL-3',
    'summary': 'Sanitize Python exception tracebacks',
    'images': [
        'static/description/main_screenshot.png',
    ],
    'application': False,
    'auto_install': False,
    'installable': True,
}
