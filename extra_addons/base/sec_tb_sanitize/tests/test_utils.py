# -*- coding: utf-8 -*-
# Copyright 2017 Naglis Jonaitis
# License AGPL-3 or later (https://www.gnu.org/licenses/agpl).

import os
import traceback
import unittest

from ..utils import sanitize_traceback


class TestUtils(unittest.TestCase):

    def test_sanitize_traceback_removes_full_paths(self):
        filename = os.path.basename(__file__)
        try:
            raise ValueError()
        except ValueError:
            sanitized_tb = sanitize_traceback(traceback.format_exc())
            self.assertIn(filename, sanitized_tb)
            self.assertNotIn(__file__, sanitized_tb)

    def test_sanitize_traceback_non_ascii_filenames(self):
        tb = (' File "a/b/c.py", line 42\n'
              'foo\n'
              ' File "å/b/c.py", line 22\n'
              'foo\n'
              ' File "å/b/ć.py", line 314')
        sanitized_tb = sanitize_traceback(tb)
        self.assertIn('File "ć.py"', sanitized_tb)

    def test_sanitize_traceback_no_filename(self):
        tb = (' File "<stdin>", line 1\n'
              '   ,\n'
              '   ^\n'
              'SyntaxError: invalid syntax')
        sanitized_tb = sanitize_traceback(tb)
        self.assertIn('File "<stdin>"', sanitized_tb)
