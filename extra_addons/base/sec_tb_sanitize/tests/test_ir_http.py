# -*- coding: utf-8 -*-
# Copyright 2017 Naglis Jonaitis
# License AGPL-3 or later (https://www.gnu.org/licenses/agpl).

import os

from odoo.tests import common

from ..models.ir_http import (
    install_serialize_exception_wrapper,
    uninstall_serialize_exception_wrapper,
)


class TestIrHttp(common.TransactionCase):

    def tearDown(self):
        super(TestIrHttp, self).tearDown()
        uninstall_serialize_exception_wrapper()

    def test_wrapper_not_installed_twice(self):
        '''
        Test that the traceback sanitization wrapper is installed only once.
        '''
        self.assertTrue(install_serialize_exception_wrapper())
        self.assertFalse(install_serialize_exception_wrapper())

    def test_wrapper_uninstall(self):
        '''
        Test that the traceback sanitization wrapper in uninstalled correctly.
        '''
        self.assertTrue(install_serialize_exception_wrapper())
        self.assertTrue(uninstall_serialize_exception_wrapper())
        self.assertFalse(uninstall_serialize_exception_wrapper())

    def test_serialize_exception_wrapper(self):
        install_serialize_exception_wrapper()
        from odoo.http import serialize_exception
        filename = os.path.basename(__file__)
        try:
            raise ValueError()
        except ValueError as e:
            result = serialize_exception(e)
            tb = result['debug']
        self.assertIn(filename, tb)
        self.assertNotIn(__file__, tb)
