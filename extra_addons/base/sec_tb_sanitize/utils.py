# -*- coding: utf-8 -*-
# Copyright 2017 Naglis Jonaitis
# License AGPL-3 or later (https://www.gnu.org/licenses/agpl).

import os
import re

TB_MODULE_RE = re.compile(r'^(?P<start>\s*File\s)"(?P<path>.+)"')


def sanitize_repl(match_obj):
    fn = os.path.basename(match_obj.group('path')) or '***'
    return '{g[start]}"{fn}"'.format(g=match_obj.groupdict(), fn=fn)


def sanitize_traceback(traceback):
    return ''.join(
        TB_MODULE_RE.sub(sanitize_repl, line)
        for line in traceback.splitlines(True))
