# -*- coding: utf-8 -*-
{
    'name': 'Ukraine - Regions',
    'description': 'This module allows to select Ukrainian regions.',
    'author': 'ERP Ukraine',
    'website': 'https://erp.co.ua',
    'support': 'support@erp.co.ua',
    'category': 'Localization',
    'version': '1.0',
    'depends': ['base'],
    'data': [
        'views/res.country.state.csv',
        'views/res_company_data_ua.xml',
    ],
}
