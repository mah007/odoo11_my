# -*- coding: utf-8 -*-
{
    'name': "Product Based On Pricelist",
    'version': "11.0.0.1",
    'summary': "This module allows you to select only those product which is assigned to the pricelist.",
    'category': 'Sale',
    'description': """
         This module allows you to select only those product which is assigned to the pricelist.
         sale pricelist
         product restricted with pricelist
         product select based on pricelist 
    """,
    'author': "Sitaram",
    'website': " ",
    'depends': ['base','sale_management','product'],
    'data': [
    ],
    'demo': [],
    "license": "AGPL-3",
    'images':['static/description/banner.png'],
    'live_test_url':'https://youtu.be/lZOpCcY734E',
    'installable': True,
    'application': True,
    'auto_install': False,
}
