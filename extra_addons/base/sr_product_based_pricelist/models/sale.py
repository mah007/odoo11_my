from odoo import api, fields, models

class product_product(models.Model):
    _inherit = "product.product"

    @api.multi
    def name_get(self):
        product_list = []
        pricelist = self.env['product.pricelist'].browse(self._context.get('pricelist'))
        if pricelist:
            for record in pricelist.item_ids:
                if record.applied_on == '0_product_variant':
                    product_list.append(record.product_id.id)
        if product_list:
            self = self.browse(product_list)
        result = super(product_product,self).name_get()
        return result
        
        

