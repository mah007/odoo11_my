# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2018-Today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from odoo import api, fields, models, _
from datetime import date, datetime

#Class is Extended for new feature for create new menu of employee.shift model.
class EmployeeShift(models.Model):
    _name = "employee.shift"
    _rec_name = "shift_detail"

    shift_detail = fields.Char("Shift")
    sequence_number = fields.Char("Sequence")
    calendar_id = fields.Many2one('resource.calendar',"Working Time")

    #Create sequence number.
    @api.model
    def create(self,vals):
        sequence = self.env['ir.sequence'].sudo().get('employee.shift') or ' '
        vals['sequence_number'] = sequence
        return super(EmployeeShift, self).create(vals)  

     
#Class is Extended for new feature for update next shift in employee.
class HrEmployee(models.Model):
    _inherit = "hr.employee"

    shift_detail = fields.Many2one("employee.shift","Shift")

    def _update_latest_shift(self,cron_mode=True):
        if date.today().weekday() == 6:
            sequence_list = []

            shift_details = self.env['employee.shift'].search([])
            for shift in shift_details:
                if shift:
                    sequence_list.append(shift.sequence_number)
            sequence_list.sort()
    
            employee_ids = self.env['hr.employee'].search([])
            for employee_id in employee_ids:
                if employee_id.shift_detail.sequence_number:
                    current_sequence = employee_id.shift_detail.sequence_number
    
                    if len(sequence_list) > (sequence_list.index(current_sequence) + 1):
                        next_sequence = sequence_list[(sequence_list.index(current_sequence) + 1)]
                        next_shift = self.env['employee.shift'].search([('sequence_number','=',next_sequence)])
                        if next_shift:
                            next_working_time = next_shift.calendar_id
                            employee_id.sudo().write({'shift_detail' : next_shift.id, 'calendar_id':next_working_time.id})
                                
                    elif len(sequence_list) == (sequence_list.index(current_sequence) + 1):
                        next_sequence = sequence_list[0]
                        next_shift = self.env['employee.shift'].search([('sequence_number','=',next_sequence)])
                        if next_shift:
                            next_working_time = next_shift.calendar_id
                            employee_id.sudo().write({'shift_detail' : next_shift.id, 'calendar_id':next_working_time.id})


