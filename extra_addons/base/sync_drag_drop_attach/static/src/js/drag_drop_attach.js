odoo.define('sync_drag_drop_attach.drag_drop_attach', function (require) {
"use strict";

    var core = require('web.core');
    var framework = require('web.framework');
    var FormRenderer = require('web.FormRenderer');
    var FormController = require('web.FormController');

    FormRenderer.include({
        _updateView: function ($newContent) {
            var self = this;
            this._super($newContent);
            self.$el.find('.o_form_sheet').before($('<div class="hidden_formview_drop"><span class="msg_content">Drop your files here</span></div>'));
        },
    });

    FormController.include({
        _updateButtons: function () {
            var self = this;
            this._super.apply(this, arguments);
            if(this.mode === "readonly") {
                self.$el.find('div.o_form_sheet').on('dragover',function(e) {
                    e.preventDefault();e.stopPropagation();
                    self.$el.find('.o_form_sheet_bg').closest('.o_content').addClass('o_drop_mode');
                    self.$el.find('.o_form_sheet').addClass('adjust_sheet');
                    self.$el.find('div.hidden_formview_drop').addClass('hidden_drop_formview_highlight');
                })
                .on('dragleave', function(e){
                    e.preventDefault();e.stopPropagation();
                    self.toggle_effect(e)
                })
                .off('drop').on('drop',function(e) {
                    e.preventDefault();e.stopPropagation();
                    self.toggle_effect(e);
                    if(e.originalEvent.dataTransfer && e.originalEvent.dataTransfer.files.length){
                        framework.blockUI();
                        self.upload_files(e.originalEvent.dataTransfer.files);
                    }
                });
            } else {
                self.$el.find('div.o_form_sheet').off('dragover').off('dragleave').off('drop');
            }
        },
        toggle_effect: function(e){
            var self = this;
            e.preventDefault();e.stopPropagation();
            self.$el.find('div.hidden_formview_drop').removeClass('hidden_drop_formview_highlight');
            self.$el.find('.o_form_sheet').removeClass('adjust_sheet');
            self.$el.find('.o_form_sheet_bg').closest('.o_content').removeClass('o_drop_mode');
        },
        upload_files: function(files){
            var self = this;
            var record = this.model.get(this.handle, {raw: true});
            var flag = 1;
            var total_attachements = 0;
            _.each(files, function(file){
                var querydata = new FormData();
                querydata.append('callback', 'oe_fileupload_temp2');
                querydata.append('model', self.modelName);
                querydata.append('id', record.res_id);
                querydata.append('ufile',file);
                querydata.append('multi', 'true');
                querydata.append('csrf_token', core.csrf_token);
                $.ajax({
                    url: '/web/binary/upload_attachment',
                    type: 'POST',
                    data: querydata,
                    cache: false,
                    processData: false,
                    contentType: false,
                    success: function(id){
                        self.saveRecord();
                        if(files.length == flag) framework.unblockUI();
                        flag += 1;
                    }
                });
            });
        }
    })
});