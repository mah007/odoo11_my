# -*- coding: utf-8 -*-

{
    'name': 'Display Address Type',
    'version': '1.0',
    'summary': 'Partner Address Type Display',
    'sequence': 1,
    'description': """
Display Address Type
====================
    * Manage the partner contacts address type.
      - Shipping Address, Invoice Address, Other Contacts.
    * Allow to filter or group by address type(Ex.shipping,invoice).
    """,
    'author': 'AVP Technolabs',
    'company': 'AVP Technolabs',
    'website': 'https://www.avptechnolabs.com',
    'license': 'AGPL-3',
    'category': 'Partner',
    'images': ["static/description/banner.png"],
    'depends': ['contacts'],
    'data': [
        'views/res_partner_view.xml',
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
