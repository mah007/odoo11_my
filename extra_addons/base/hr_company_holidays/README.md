
Author : FOSS INFOTECH PVT LTD

Module : HR\_company\_holidays

Version : 11

<h2>HR company holidays</h2>

<p> HR company holiday module for Odoo 11. This modules helps you create a list of company holidays.</p>

<b>Step 1</b>:  Once the module is installed, Go to 'Employees'.
<img src="static/description/0.png">

<b>Step 2</b>: Hit on the create button and create the holiday.
<img src="static/description/1.png">

<b>Step 3</b>:Go to Calendar menu, Find the scheduled holiday reflecting now in the calendar.
<img src="static/description/2.png">

