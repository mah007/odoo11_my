# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Merge Tasks',
    'version': '11.0.0.1',
    'summary': 'This modules helps to merge tasks and it will merge timesheet of tasks too',
    'description': """
        project tasks merge
        task merger
        merge tasks project 
        project merge taskss
        task merge
    """,
    'author': 'BrowseInfo',
    'website': 'http://www.browseinfo.in',
    'depends': ['base','project','hr_timesheet'],
    'data': [
            'view/task.xml'
             ],
	'qweb': [
		],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    "images":['static/description/Banner.png'],
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
