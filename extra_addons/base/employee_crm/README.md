Quick access to opportunity pipeline of the employee
----------------------------------------------------

This module is a step forward to allow the user to have a quick access to analysed information at the right place.
This module helps any user to see the number of opportunity pipeline with stages on the employee.
