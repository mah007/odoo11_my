# -*- coding: utf-8 -*-

from odoo import models, fields


class Partner(models.Model):
    _inherit = 'res.partner'

    company_registry = fields.Char('Company Registry' , required=False)
