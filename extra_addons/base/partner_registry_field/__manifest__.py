{
    'name': 'Company Registry field in Partner form',
    'images': ['images/main_screenshot.png'],
    'version': '1.0.1',
    'category': 'Partner',
    'summary': 'Adds new field Company Registry Code',
    'author': 'Inovacijos',
    'website': 'http://www.innovations.lt',
    'depends': [
    ],
    'installable': True,
    'license': 'AGPL-3',
    'data': [
        'views/partner_views.xml',
    ],
}
