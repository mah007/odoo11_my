# -*- coding: utf-8 -*-


{
    'name': 'Upper case Customers/Suppliers/Products/Entrepôts & Unicity',
    'version': '1.1',
    'category': 'Sale',
    'sequence': 200,
    'summary': 'Transformation min/maj, unicité',
    'description': """
    Module complémentaire    """,
    'author': 'Odoo Tips',
    "website" : "http://www.gotodoo.ma",
    'depends': ['base','sale','purchase','stock'],
    'images': ['images/main_screenshot.png'],
    'data': [ ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
