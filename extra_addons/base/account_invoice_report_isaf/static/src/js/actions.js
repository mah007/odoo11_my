odoo.define('account_invoice_report_isaf', function (require) {
"use strict";

var man = require('web.ActionManager');
var session = require('web.session');
var framework = require('web.framework');

man.include({
            ir_actions_act_url: function (action, options) {
                var url = action.url;

                if (session.debug && url && url.length && url[0] === '/')
                        url = $.param.querystring(url, {debug: session.debug});

                if (action.target === 'self') {
                        framework.redirect(url);
                        return $.Deferred(); // The action is finished only when the redirection is done
                } else if (action.target === 'download') {
                        framework.redirect(url);
		} else {
                    window.open(url, '_blank');
                    options.on_close();
                }
                return $.when();
            }
    })
});

