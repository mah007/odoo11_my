{
    'name': 'i.SAF XML report',
    'images': ['images/main_screenshot.png'],
    'version': '1.0.8',
    'category': 'Invoice',
    'summary': 'Creates an i.SAF report for Lithuanian State Tax Inspectorate',
    'author': 'Inovacijos',
    'website': 'http://www.innovations.lt',
    'depends': [
	'account',
        'account_tax_class',
        'partner_registry_field',
    ],
    'installable': True,
    'license': 'AGPL-3',
    'data': [
        'views/wizard.xml',
    ],
}
