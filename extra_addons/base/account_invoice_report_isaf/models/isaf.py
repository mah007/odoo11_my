
from odoo import models, fields, api
import base64
from lxml import etree
from datetime import date, timedelta, datetime
from odoo.exceptions import Warning
from odoo.tools.translate import _


class Wizard(models.Model):
   _name = 'isaf.wizard'

   when = date.today()
   this_first = date(when.year, when.month, 1)
   prev_end = this_first - timedelta(days=1)
   prev_first = date(prev_end.year, prev_end.month, 1)

   start_date = fields.Date('Start Date' , required=True, default=prev_first)
   end_date = fields.Date('End Date' , required=True, default=prev_end)
   scope = fields.Selection(string='Scope', required=True, selection=[('sale', 'Sales'), ('purchase', 'Purchases')], default='sale')
   norm = fields.Boolean('Normalize numbers', help='Strip spaces and "-" chars from invoice numbers and references', default=True)

   @api.onchange('scope')
   def scope_onchange(self):
     self.journal_ids = self.get_journals(self.scope)
     return {'domain': {'journal_ids': [('type', '=', self.scope)]}}

   @api.multi
   def get_journals(self, scope='sale'):
     return self.env['account.journal'].search([('type', '=', scope)]).ids

   journal_ids = fields.Many2many('account.journal', string='Journals', domain="[('type', '=', 'sale')]", required=True, default=get_journals)

   def normalize(self, number):
     if self.norm:
       return number.replace(' ', '').replace('-', '')
     return number

   def partner_info(self, partner_id, info):
     PartnerInfo = etree.Element(info)
     PartnerID = etree.Element(info.replace('Info', '')+'ID')
     PartnerID.text = partner_id.ref
     PartnerInfo.append(PartnerID)
     VATRegistrationNumber = etree.Element('VATRegistrationNumber')
     if partner_id.vat:
        VATRegistrationNumber.text = partner_id.vat
     else:
        VATRegistrationNumber.text = 'ND'
     PartnerInfo.append(VATRegistrationNumber)
     RegistrationNumber = etree.Element('RegistrationNumber')
 
     if 'x_reg' in self.env['res.partner']._fields:
       x_reg = partner_id.x_reg
     else:
       x_reg = partner_id.company_registry

     if x_reg:
        RegistrationNumber.text = x_reg
     else:
        RegistrationNumber.text = 'ND'
     PartnerInfo.append(RegistrationNumber)
     Country = etree.Element('Country')
     if partner_id.country_id.code:
        Country.text = partner_id.country_id.code
     else:
        Country.text = 'ND'
     PartnerInfo.append(Country)
     Name = etree.Element('Name')
     Name.text = partner_id.name
     PartnerInfo.append(Name)
     return PartnerInfo

   def document_total(self, invoice, taxes):
     DocumentTotal = etree.Element('DocumentTotal')
     TaxCode = etree.Element('TaxCode')
     TaxCode.text = taxes.class_code.code

     TaxableValue = etree.Element('TaxableValue')
     TaxableValue.text = str(invoice.amount_untaxed_signed)

     Amount = etree.Element('Amount')
     TaxPercentage = etree.Element('TaxPercentage')

     if taxes.class_code.non_standard:
       TaxPercentage.set('nil', 'true')
     else:
       TaxPercentage.text = str(int(taxes.amount))

     Amount.text = str(invoice.amount_tax)
     DocumentTotal.append(TaxableValue)
     DocumentTotal.append(TaxCode)
     DocumentTotal.append(TaxPercentage)
     DocumentTotal.append(Amount)
     
     if self.scope == 'sale':
       VATPointDate2 = etree.Element('VATPointDate2')
       VATPointDate2.set('nil', 'true')
       DocumentTotal.append(VATPointDate2)
     return DocumentTotal

   @api.multi
   def action_download(self):
      if not 'x_reg' in self.env['res.partner']._fields and not 'company_registry' in self.env['res.partner']._fields:
        raise Warning(_('Error: Module "partner_registry_field" not found. Aborting'))
      if not 'class_code' in self.env['account.tax']._fields:
        raise Warning(_('Error: Module "account_tax_class" not found. Aborting'))

      invoice_type = 'out_invoice'
      ScopePartners = 'Customers'
      ScopePartner = 'Customer'
      ScopeInvoices = 'SalesInvoices'
      ScopePartnerInfo = 'CustomerInfo'
      if self.scope == 'purchase':
        invoice_type = 'in_invoice'
        ScopePartners = 'Suppliers'
        ScopePartner = 'Supplier'
        ScopeInvoices = 'PurchaseInvoices'
        ScopePartnerInfo = 'SupplierInfo'
      
      domain = [('date_invoice', '>=', self.start_date), ('date_invoice', '<=', self.end_date), ('type', '=', invoice_type), ('state', 'in', ['open', 'paid']), ('journal_id', 'in', self.journal_ids.ids)]
      invoices = self.env['account.invoice'].search(domain)

      xsi = etree.Element("{http://www.w3.org/2001/XMLSchema-instance}xsi")
      iSAFFile = etree.SubElement(xsi,'iSAFFile')

      iSAFFile.set('xmlns', 'http://www.vmi.lt/cms/imas/isaf')
      Header = etree.Element('Header')
      FileDescription = etree.Element('FileDescription')

      FileVersion = etree.Element('FileVersion')
      FileVersion.text = 'iSAF1.2'
      FileDateCreated = etree.Element('FileDateCreated')
      FileDateCreated.text = str(datetime.now().strftime("%Y-%m-%dT%H:%M:01Z"))
      DataType = etree.Element('DataType')
      DataType.text = 'F'
      SoftwareCompanyName = etree.Element('SoftwareCompanyName')
      SoftwareCompanyName.text = 'Inovacijos'
      SoftwareName = etree.Element('SoftwareName')
      SoftwareName.text = 'i.SAF XML report'
      SoftwareVersion = etree.Element('SoftwareVersion')
      SoftwareVersion.text = '1.0'
      RegistrationNumber = etree.Element('RegistrationNumber')
      RegistrationNumber.text = self.env.user.company_id.company_registry
      NumberOfParts = etree.Element('NumberOfParts')
      NumberOfParts.text = '1'
      PartNumber = etree.Element('PartNumber')
      PartNumber.text = '1'
      SelectionCriteria = etree.Element('SelectionCriteria')
      SelectionStartDate = etree.Element('SelectionStartDate')
      SelectionEndDate = etree.Element('SelectionEndDate')
      SelectionStartDate.text = self.start_date
      SelectionEndDate.text = self.end_date
      SelectionCriteria.append(SelectionStartDate)
      SelectionCriteria.append(SelectionEndDate)

      Header.append(FileDescription)
      FileDescription.append(FileVersion)
      FileDescription.append(FileDateCreated)
      FileDescription.append(DataType)
      FileDescription.append(SoftwareCompanyName)
      FileDescription.append(SoftwareName)
      FileDescription.append(SoftwareVersion)
      FileDescription.append(RegistrationNumber)
      FileDescription.append(NumberOfParts)
      FileDescription.append(PartNumber)
      FileDescription.append(SelectionCriteria)

      iSAFFile.append(Header)
      
      MasterFiles = etree.Element('MasterFiles')  
      iSAFFile.append(MasterFiles)

      Partners = etree.Element(ScopePartners)  
      MasterFiles.append(Partners)

      partner_ids = []

      SourceDocuments = etree.Element('SourceDocuments')
      iSAFFile.append(SourceDocuments)

      Invoices = etree.Element(ScopeInvoices)
      SourceDocuments.append(Invoices)

      for invoice in invoices:
        partner_ids.insert(0, invoice.partner_id.id)
        Invoice = etree.Element('Invoice')
        Invoices.append(Invoice)
        InvoiceNo = etree.Element('InvoiceNo')
        if self.scope == 'purchase':
          InvoiceNo.text = self.normalize(invoice.reference)
        else:
          InvoiceNo.text = self.normalize(invoice.number)
        Invoice.append(InvoiceNo)
        PartnerInfo = self.partner_info(invoice.partner_id, ScopePartnerInfo)
        Invoice.append(PartnerInfo)

        InvoiceDate = etree.Element('InvoiceDate')
        InvoiceDate.text = invoice.date_invoice
        Invoice.append(InvoiceDate)

        InvoiceType = etree.Element('InvoiceType')
        Invoice.append(InvoiceType)

        SpecialTaxation = etree.Element('SpecialTaxation')
        Invoice.append(SpecialTaxation)

        References = etree.Element('References')
        Invoice.append(References)
      

        VATPointDate = etree.Element('VATPointDate')
        VATPointDate.set('nil', 'true')
        Invoice.append(VATPointDate)
 
        if self.scope == 'purchase':
          RegistrationAccountDate = etree.Element('RegistrationAccountDate')
          RegistrationAccountDate.text = invoice.date_invoice
          Invoice.append(RegistrationAccountDate)

        DocumentTotals = etree.Element('DocumentTotals')

        tax_ids = invoice.tax_line_ids
        tax_record = []
        for tax_id in tax_ids:
          tax_record.insert(0, tax_id.tax_id.id)

        taxes = self.env['account.tax'].search([('id', 'in', tax_record)])

        if len(taxes) == 1:
          DocumentTotal = self.document_total(invoice, taxes)
          DocumentTotals.append(DocumentTotal)
        else:
          for tax in taxes:
            continue
        
        Invoice.append(DocumentTotals)

      partners = self.env['res.partner'].search([('id', 'in', partner_ids)])
      for partner in partners:
       Partner  = self.partner_info(partner, ScopePartner)
       Partners.append(Partner)

      xmlstr = etree.tostring(iSAFFile, encoding='utf-8', method='xml').decode().replace('nil="true"/','xsi:nil="true"/').encode('utf-8')
      xmlstr = str('<?xml version="1.0" encoding="UTF-8"?>').encode('utf-8') + xmlstr

      values = {           
         'name': "i.SAF-report-"+invoice_type,           
         'datas_fname': 'iSAF-'+invoice_type+'.xml',           
         'res_model': 'ir.ui.view',          
         'res_id': False,           
         'type': 'binary',          
         'public': False,           
         'datas': base64.b64encode(xmlstr),       
     }

      attachment_id = self.env['ir.attachment'].sudo().create(values)
      download_url = '/web/content/' + str(attachment_id.id) + '?download=True'
      base_url = self.env['ir.config_parameter'].get_param('web.base.url')
      return {       
            'type'     : 'ir.actions.act_url',
            'url'      : str(download_url),      
            'target'   : 'download',    
      }
