{
    'name': 'Partner Country mandatory',
    'images': ['images/main_screenshot.png'],
    'version': '1.0.1',
    'category': 'Partner',
    'summary': 'Makes Country field mandatory in the Partner form',
    'author': 'Inovacijos',
    'website': 'http://www.innovations.lt',
    'depends': [
    ],
    'installable': True,
    'license': 'AGPL-3',
    'data': [
    ],
}
