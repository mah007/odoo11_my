# -*- encoding : utf-8 -*-
from odoo import api, fields, models, _

class PosConfig(models.Model):
    _inherit = 'pos.config'

    is_list_view = fields.Boolean(string='List View', help='Enable list view in Point of Sale (disable load product pictures)')