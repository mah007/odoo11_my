# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
{
  "name"                 :  "Website Product Sortby & Shopby",
  "summary"              :  "Make Shopping Easy by introducing shop-by and sort-by feature on your website.",
  "category"             :  "Website",
  "version"              :  "0.2",
  "sequence"             :  1,
  "author"               :  "Webkul Software Pvt. Ltd.",
  "license"              :  "Other proprietary",
  "maintainer"           :  "Prakash Kumar",
  "website"              :  "https://store.webkul.com/Odoo-Website-Product-SortBy-ShopBy.html",
  "description"          :  """https://webkul.com/blog/website-product-sortby-shopby-2
  Make Shopping Easy by introducing shop-by and sort-by feature on your website.
  """,
  "live_test_url"        :  "http://odoodemo.webkul.com/?module=website_product_sorting_and_shopping&version=11.0",
  "depends"              :  ['website_product_brands'],
  "data"                 :  [
                             'views/template.xml',
                             'views/brands_template.xml',
                             'views/category_template.xml',
                             'views/sort_by_shop_by.xml',
                             'security/ir.model.access.csv',
                             'data/data.xml',
                            ],
  "images"               :  ['static/description/Banner.png'],
  "application"          :  True,
  "installable"          :  True,
  "price"                :  24,
  "currency"             :  "EUR",
  "pre_init_hook"        :  "pre_init_check",
}
