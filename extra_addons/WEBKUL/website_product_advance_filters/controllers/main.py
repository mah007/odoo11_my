# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# License URL : https://store.webkul.com/license.html/
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
from odoo import http, _
from odoo.http import request
from odoo.addons.website_sale.controllers.main import WebsiteSale
from odoo.addons.website.controllers.main import QueryURL

import logging
_logger = logging.getLogger(__name__)

class WebsiteSale(WebsiteSale):

    def _get_search_domain(self, search, category, attrib_values):
        product_ids = False
        res = super(WebsiteSale,self)._get_search_domain(search, category, attrib_values)
        filter_list = request.httprequest.args.getlist('filter')
        filter_value_ids = [int(v) for v in filter_list if v]
        if filter_value_ids:
            filter_value_objs = request.env["product.filter.value"].browse(filter_value_ids)
            product_ids = filter_value_objs.mapped('product_ids')
            product_ids = product_ids.ids if product_ids else []
            res+=[('id','in',product_ids)]
        return res


    @http.route([
        '/shop',
        '/shop/page/<int:page>',
        '/shop/category/<model("product.public.category"):category>',
        '/shop/category/<model("product.public.category"):category>/page/<int:page>'
    ], type='http', auth="public", website=True)
    def shop(self, page=0, category=None, search='', ppg=False, **post):
        filter_ids = None
        ProductFilter = request.env['product.filter']
        filter_value_objs = False
        sel_fltr_name = {}

        filter_list = request.httprequest.args.getlist('filter')
        filter_value_ids = [int(v) for v in filter_list if v]

        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [[int(x) for x in v.split("-")] for v in attrib_list if v]

        if filter_value_ids:
            filter_value_objs = request.env["product.filter.value"].browse(filter_value_ids)
            for rec in filter_value_objs:
                data = sel_fltr_name.get(rec.filter_id.id,False)
                if data:
                    sel_fltr_name[rec.filter_id.id] = data+rec.name + ','
                else:
                    sel_fltr_name[rec.filter_id.id] = rec.name + ','

        filter_ids = ProductFilter.search([('value_ids','!=',False)])
        if category and category.filter_ids:
            filter_ids = ProductFilter.search([('value_ids','!=',False),('filter_activation','=','all_cat')])
            temp_fltr_ids = filter_ids.ids if filter_ids else None
            temp_fltr_ids = set(temp_fltr_ids + category.filter_ids.ids)
            filter_ids = ProductFilter.browse(temp_fltr_ids)

        keep = QueryURL('/shop', category=category and int(category), search=search, attrib=attrib_list, filter=filter_list, order=post.get('order'))

        res = super(WebsiteSale, self).shop(page, category, search, ppg, **post)
        res.qcontext.update({
            'filter_ids': filter_ids if filter_ids else [],
            'sel_fltr_name' : sel_fltr_name if sel_fltr_name else False,
            'selected_value_ids' : filter_value_objs,
            'keep': keep,
        })
        return res
