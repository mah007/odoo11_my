/* Copyright (c) 2016-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>) */
/* See LICENSE file for full copyright and licensing details. */
/* @License       : https://store.webkul.com/license.html */

odoo.define('website_product_advance_filters.product_filters', function(require) {
    "use strict";

    var ajax = require('web.ajax');
    var core = require('web.core');
    var _t = core._t;

    $(document).ready(function() {

        $('form.js_p_filters input').on('change', function (event) {
            if (!event.isDefaultPrevented()) {
                event.preventDefault();
                $(this).closest("form").submit();
            }
        });

        $('.p_filter_header').on('click',function(){
            var $this = $(this);
            var $icon = $this.find('.p_filter_icon');
            if($icon.hasClass('fa-plus'))
            {
                $icon.removeClass('fa-plus')
                $icon.addClass('fa-minus')
            }
            else{
                if($icon.hasClass('fa-minus'))
                {
                    $icon.removeClass('fa-minus')
                    $icon.addClass('fa-plus')
                }
            }
            $this
                .next()
                    .slideToggle();
        });

        $('.remove_fltr_value').on('click',function(){
            var remove_fltr_value_id = String($(this).data("remove_fltr_value"))
            var redirect_url = window.location.href
            var rem_fltr = "filter=" + remove_fltr_value_id
            if(redirect_url.indexOf(rem_fltr+"&") != -1){
                redirect_url = redirect_url.replace(new RegExp(rem_fltr+"&"), '');
            }
            else if(redirect_url.endsWith(rem_fltr)){
                redirect_url = redirect_url.replace(new RegExp('[?&]'+rem_fltr+'$'), '');
            }
            if(redirect_url.endsWith("?")){
                redirect_url = redirect_url.slice(0, -1);
            }
            window.location.href = redirect_url;
        });
    });
});
