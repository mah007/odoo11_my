# -*- coding: utf-8 -*-
##########################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2016-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# License URL :<https://store.webkul.com/license.html/>
##########################################################################
from odoo import models, fields, api, _
from odoo.osv import expression
from odoo.tools.safe_eval import safe_eval

import logging
_logger = logging.getLogger(__name__)

class ProductPublicCategory(models.Model):
    _inherit = "product.public.category"

    filter_ids = fields.Many2many("product.filter", string="Product Filters")

class ProductTemplate(models.Model):
    _inherit = "product.template"

    filter_value_ids = fields.Many2many("product.filter.value", String="Filter value")

class ProductFilter(models.Model):
    _name = "product.filter"

    name = fields.Char("Title")
    value_ids = fields.One2many("product.filter.value", "filter_id", string="Filter Value")
    category_ids = fields.Many2many("product.public.category", string="Categories")
    default_fold = fields.Boolean(string="Fold", default=True)
    filter_activation = fields.Selection(selection=[('all_cat','All Categories'),('sel_cat','Selected Categories')], default="all_cat", string="Active For")

    @api.multi
    def toggle_default_fold(self):
        """ Inverse the value of the field ``default_fold`` on the records in ``self``. """
        for record in self:
            record.default_fold = not record.default_fold

class FilterValues(models.Model):
    _name = "product.filter.value"

    name = fields.Char(string="Name")
    filter_id = fields.Many2one("product.filter", string="Filter")
    product_ids = fields.Many2many("product.template", string="Products")
    applied_config = fields.Selection(selection=[('manual','Manually select products'),('auto','Automatically select products based on conditions')], default="manual", string="Options")
    product_count = fields.Integer(string="Selected Products", compute="_count_product_template_ids")
    product_domain = fields.Char("Product Domain")

    @api.depends('product_ids')
    def _count_product_template_ids(self):
        for rec in self:
            rec.product_count = len(rec.product_ids.ids)

    @api.onchange('applied_config')
    def on_change_applied_config(self):
        self.product_ids = [(6, 0, [])]
        self.product_domain = None

    def get_all_products(self):
        domain = safe_eval(self.product_domain)
        product_objs = self.env["product.template"].search(domain)
        return product_objs.ids

    @api.onchange('product_domain')
    def on_change_product_domain(self):
        if self.applied_config == 'auto':
            product_ids = self.get_all_products()
            self.product_ids = [(6, 0, product_ids)]

    @api.model
    def create(self, vals):
        rec = super(FilterValues, self).create(vals)
        rec.on_change_product_domain()
        return rec

    @api.multi
    def write(self, vals):
        res = super(FilterValues, self).write(vals)
        for obj in self:
            if obj.applied_config == "auto":
                product_ids = self.get_all_products()
                vals["product_ids"] = [(6, 0, product_ids)]
                res = super(FilterValues, obj).write(vals)
        return res

    @api.multi
    @api.depends('name', 'filter_id')
    def name_get(self):
        result = []
        for rec in self:
            name = rec.filter_id.name + '-' + rec.name
            result.append((rec.id, name))
        return result
