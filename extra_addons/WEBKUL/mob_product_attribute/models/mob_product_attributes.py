# -*- coding: utf-8 -*-
##########################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   "License URL : <https://store.webkul.com/license.html/>"
#
##########################################################################

import time
import datetime
try:
    from xmlrpc import client as xmlrpclib
except ImportError:
    import xmlrpclib

from odoo import api, fields, models, _
from odoo.exceptions import UserError

import logging
_logger = logging.getLogger(__name__)

# .............magento-product custom option/attribute...


class ProductCustomOptions(models.Model):
    _name = "product.custom.options"
    _description = "Magento Custom Options"
    _order = 'id desc'

    name = fields.Char(string='Title', size=100)
    values = fields.Char(string='Values', size=100)
    mage_id = fields.Integer(string='Magento Option Id', size=100)
    create_date = fields.Datetime(string='Created Date')
    write_date = fields.Datetime(string='Updated Date')
    fkey2_product = fields.Many2one('product.template', string='Product')

    @api.model
    def _balance_custom_options(self, currentOptions, tmplId):
        previousOptions = []
        existObjs = self.search([('fkey2_product', '=', tmplId)])
        if existObjs:
            for obj in existObjs:
                previousOptions.append((obj.id, obj.mage_id))
        unlinkOptions = list(set(previousOptions) - set(currentOptions))
        if unlinkOptions:
            unlinkIds = []
            for u in unlinkOptions:
                unlinkIds.append(u[0])
            unlinkObjs = self.browse(unlinkIds)
            unlinkObjs.unlink()
        return True

    @api.model
    def link_product_with_custom_options(self, data, productId):
        customDict = {}
        currentOptions = []
        tmplId = self.env['product.product'].browse(
            productId).product_tmpl_id.id
        for d in data:
            customDict['mage_id'] = optId = d['option_id']
            customDict['name'] = title = d['title']
            customDict['values'] = values = d['values']
            customDict['fkey2_product'] = tmplId
            opt = self.search(
                [('fkey2_product', '=', tmplId),
                 ('mage_id', '=', optId)])
            if not opt:
                obj = self.create(customDict)
            else:
                obj = opt[0]
                createdValues = obj.values
                if createdValues != values:
                    obj.values = values
            currentOptions.append((obj.id, optId))
        self._balance_custom_options(currentOptions, tmplId)
        return True


class ProductAttribute(models.Model):
    _name = "product.attributes"
    _description = "Magento Product Custom Attributes"
    _order = 'id desc'

    @api.depends('image')
    def _has_media(self):
        for obj in self:
            if obj.image:
                obj.has_media = True

    name = fields.Char(string='Name', size=100)
    attribute_id = fields.Many2one('magento.attributes', string='Attribute')
    value = fields.Char(string='Value', size=100)
    image = fields.Binary(string='Media Image')
    has_media = fields.Boolean(compute='_has_media')
    create_date = fields.Datetime(string='Created Date')
    write_date = fields.Datetime(string='Updated Date')
    fkey_product = fields.Many2one('product.template', string='Product')

    @api.model
    def link_product_attributes(self, data):
        attrDict = {}
        value = False
        tmplId = 0
        mageAttrId = 0
        instanceId = self._context.get('instance_id')
        inputType = data.get('input_type')
        if 'attribute_id' in data:
            mageAttrId = data['attribute_id']
        if 'product_id' in data:
            tmplId = self.env['product.product'].browse(
                data.get('product_id')).product_tmpl_id.id
            attrDict['fkey_product'] = tmplId
        if 'value' in data:
            value = data['value']
        if value and inputType == 'media_image':
            attrDict['image'] = value
        elif value:
            attrDict['value'] = value
        if mageAttrId:
            attributeObjs = self.env['magento.attributes'].search(
                [('mage_id', '=', mageAttrId),
                 ('instance_id', '=', instanceId)])
            if attributeObjs and tmplId:
                existObjs = self.search(
                    [('attribute_id', '=', attributeObjs[0].id),
                     ('fkey_product', '=', tmplId)])
                if existObjs:
                    if not value:
                        existObjs[0].unlink()
                    elif inputType == 'media_image':
                        existObjs[0].image = value
                    else:
                        attrValue = existObjs[0].value
                        if value != attrValue:
                            existObjs[0].value = value
                if not existObjs and value:
                    attrDict['attribute_id'] = attributeObjs[0].id
                    self.create(attrDict)
                return True
        return False


class MagentoAttributes(models.Model):
    _name = "magento.attributes"
    _description = "Magento Custom Attributes"
    _order = 'id desc'
    _rec_name = 'label'

    name = fields.Char(string='Attribute code', size=100)
    label = fields.Char(string='Label', size=100)
    mage_id = fields.Integer(string='Magento Attribute Id', size=100)
    instance_id = fields.Many2one(
        'magento.configure', string='Magento Instance')
    create_date = fields.Datetime(string='Created Date')
    write_date = fields.Datetime(string='Updated Date')


    @api.model
    def create(self, vals):
        ctx = dict(self._context or {})
        if ctx.get('instance_id'):
            vals.update(
                {'instance_id': ctx.get('instance_id')}
            )
        return super(MagentoAttributes, self).create(vals)
