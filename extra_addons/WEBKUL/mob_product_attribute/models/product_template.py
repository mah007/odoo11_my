# -*- coding: utf-8 -*-
##########################################################################
#
#	Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   "License URL : <https://store.webkul.com/license.html/>"
#
##########################################################################

from odoo import api, fields, models, _


# .............magento-product custom option/attribute...

class ProductTemplate(models.Model):
    _inherit = "product.template"

    attributes = fields.One2many(
        'product.attributes', 'fkey_product', string='Custom Attributes')
    custom_options = fields.One2many(
        'product.custom.options', 'fkey2_product', string='Custom Options')
