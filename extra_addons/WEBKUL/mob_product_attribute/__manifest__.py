# -*- coding: utf-8 -*-
##########################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
##########################################################################
{
    "name": "MOB Product Attribute",
    "summary": "MOB Attribute Extension",
    "category": "Generic Modules",
    "version": "2.4.1",
    "sequence": 5,
    "author": "Webkul Software Pvt. Ltd.",
    "license": "Other proprietary",
    "website": "https://store.webkul.com",
    "description": """
MOB Product Attribute Extension
===============================

Some of the brilliant feature of this extension:
------------------------------------------------
    This Module helps in maintaining Product Attributes and it's Custom Options between Magento and Odoo.

brilliant feature of the module:
--------------------------------

    1. it'll Maintain Magento Product Attributes(except Configurable).

    2. Maintain all Product Custom Options.

    3. Options value will be managed for Magento Order(inside order line).


    NOTE : This module works very well with latest version of magento 1.9.* and Odoo v11.0
  """,
    "depends": ['magento_bridge'],
    "data": [
        'security/ir.model.access.csv',
        'views/product_template_views.xml',
        'views/mob_product_attributes_view.xml',
    ],

    "application": True,
    "installable": True,
    "auto_install": False,
    "pre_init_hook": "pre_init_check",
}
