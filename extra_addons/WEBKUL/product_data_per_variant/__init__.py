# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
from . import models
from . import controller
from odoo import api, SUPERUSER_ID

def pre_init_check(cr):
    from openerp.service import common
    from openerp.exceptions import Warning
    version_info = common.exp_version()
    server_serie =version_info.get('server_serie')
    if server_serie!='11.0':raise Warning('Module support Odoo series 11.0 found {}.'.format(server_serie))
    return True

def _auto_set_name(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    productObjs = env['product.product'].search([])
    for productObj in productObjs:
        if not productObj.name:
            productObj.name = productObj.product_tmpl_id.name
        if not productObj.description_sale:
            productObj.description_sale = productObj.product_tmpl_id.description_sale
    productImageObjs = env['product.image'].search([])
    for productImageObj in productImageObjs:
        if not productImageObj.product_id:
            if productImageObj.product_tmpl_id:
                productImageObj.product_id = productImageObj.product_tmpl_id.product_variant_id.id
    return True