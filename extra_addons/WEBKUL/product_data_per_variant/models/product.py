# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
from odoo import api, fields, models

class ProductProduct(models.Model):
    _inherit = 'product.product'

    name = fields.Char('Name', index=True, required=True, translate=True)
    product_image_ids = fields.One2many('product.image', 'product_id', string='Images')
    description_sale = fields.Text(
        'Sale Description', translate=True,
        help="A description of the Product that you want to communicate to your customers. "
             "This description will be copied to every Sales Order, Delivery Order and Customer Invoice/Credit Note")

    @api.model
    def create(self, vals):
        if self._context.get('create_template_product'):
            if not vals.get('name'):
                if self._context.get('name'):
                    vals['name'] = self._context.get('name')
            if not vals.get('description_sale'):
                if self._context.get('description_sale'):
                    vals['description_sale'] = self._context.get('description_sale')
        res = super(ProductProduct, self.with_context(create_product_product=True, name=vals.get('name'), description_sale=vals.get('description_sale'))).create(vals)
        if not res.name:
            res.name = res.product_tmpl_id.name
            res.description_sale = res.product_tmpl_id.description_sale
        return res

    @api.multi
    def write(self, vals):
        res = super(ProductProduct, self).write(vals)
        if not self._context.get('update_template'):
            for obj in self:
                if len(obj.product_tmpl_id.product_variant_ids) == 1:
                    obj.product_tmpl_id.with_context(update_product=True).write({
                        'name':obj.name,
                        'description_sale':obj.description_sale,
                    })
        return res

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    @api.model
    def create(self, vals):
        if not vals.get('name'):
            if self._context.get('name'):
                vals['name'] = self._context.get('name')
        if not vals.get('description_sale'):
            if self._context.get('description_sale'):
                vals['description_sale'] = self._context.get('description_sale')
        template = super(ProductTemplate, self.with_context(create_template_product=True, name=vals.get('name'), description_sale=vals.get('description_sale'))).create(vals)
        return template

    @api.multi
    def write(self, vals):
        res = super(ProductTemplate, self).write(vals)
        if not self._context.get('update_product'):
            for obj in self:
                if len(obj.product_variant_ids) == 1:
                    obj.product_variant_ids[0].with_context(update_template=True).write({
                        'name':obj.name,
                        'description_sale':obj.description_sale,
                    })
        return res

class ProductImage(models.Model):
    _inherit = 'product.image'

    product_id = fields.Many2one('product.product', 'Related Product', copy=True)

    @api.model
    def create(self, vals):
        if vals.get('product_id'):
            product = self.env['product.product'].browse(vals['product_id'])
            vals['product_tmpl_id'] = product.product_tmpl_id.id
        elif vals.get('product_tmpl_id'):
            if not vals.get('product_id'):
                product = self.env['product.template'].browse(vals['product_tmpl_id'])
                vals['product_id'] = product.product_variant_ids[0].id
        return super(ProductImage, self).create(vals)
