/* Copyright (c) 2016-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>) */
/* See LICENSE file for full copyright and licensing details. */
odoo.define('product_data_per_variant.wk_variant', function (require) {
    "use strict";

    require('web.dom_ready');
    var base = require("web_editor.base");
    var ajax = require('web.ajax');
    var utils = require('web.utils');
    var core = require('web.core');
    var config = require('web.config');
    var _t = core._t;
    $('.oe_website_sale').each(function () {
        var oe_website_sale = this;
        $(oe_website_sale).on('change', 'input.js_variant_change, select.js_variant_change, ul[data-attribute_value_ids]', function (ev) {
            var $ul = $(ev.target).closest('.js_add_cart_variants');
            var $parent = $ul.closest('.js_product');
            var $product_id = $parent.find('.product_id').first();
            var productVarId = $product_id.val();
            var $imgObj = $(oe_website_sale).find('.carousel-indicators').find('li');
            var indCount = 0;
            $imgObj.each(function(){
                var proId = $(this).attr('product_id');
                if (proId) {
                    if (proId == productVarId) {
                        $(this).css({'display':'inline-block'});
                        $(this).attr('data-slide-to',indCount);
                        indCount = indCount + 1;
                    } else {
                        $(this).css({'display':'none'});
                    }
                }
            });
            var $imgVarObj = $(oe_website_sale).find('.carousel-inner').find('div');
            $imgVarObj.each(function(){
                var proVarId = $(this).attr('product_id');
                if (proVarId) {
                    if (proVarId == productVarId) {
                        $(this).css({'display':''});
                        $(this).addClass('item');
                    } else {
                        $(this).css({'display':'none'});
                        $(this).removeClass('item');
                    }
                }
            });

            var $imgVarObj = $(oe_website_sale).find('.wk_item');
            $imgVarObj.each(function(){
                var proVarId = $(this).attr('product_id');
                if (proVarId) {
                    if (proVarId == productVarId) {
                        $(this).css({'display':''});
                    } else {
                        $(this).css({'display':'none'});
                    }
                }
            });

            if ($product_id.val()) {
                ajax.jsonRpc("/product/detail", 'call', {product_id: $product_id.val()}).then(function(result){
                        if (result[0]) {
                            $('#product_details h1').text(result[0]);
                        }
                        if (result[1]) {
                            $('#hr_product_attributes_simple').prev('p').text(result[1]);
                        }
                    }
                );
            }
        });
        
    });
});