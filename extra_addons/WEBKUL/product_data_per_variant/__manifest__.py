#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################

{
    'name'          : "Website Product Data Per Variant",
    'version'       : '1.0',
    'summary'       : """Website Product Data Per Variant""",
    'author'        : 'Webkul Software Pvt. Ltd.',
    'website'       : 'https://store.webkul.com/Website-Product-Data-Per-Per-Variant.html',
    "license"       :  "Other proprietary",
    'category'      : 'website',
    "live_test_url" : "http://odoodemo.webkul.com/?module=product_data_per_variant&version=11.0&custom_url=/shop/product/e-com10-apple-wireless-keyboard-18",
    'description'   : "https://webkul.com/blog/website-product-data-per-variant",
    'depends'       : [
                        'website_sale',
                      ],
    'data'          : [
                        'views/product_view.xml',
                        'views/template.xml',
                      ],
    'demo'          : [
                      ],
    "images"        :  ['static/description/Banner.png'],
    "application"   :  True,
    "installable"   :  True,
    "auto_install"  :  False,
    "price"         :  25,
    "currency"      :  "EUR",
    'sequence'      :   1,
    'pre_init_hook' :   'pre_init_check',
    'post_init_hook': '_auto_set_name',
}