# -*- coding: utf-8 -*-
##########################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
##########################################################################
{
    "name": "MOB Customer Pricelist",
    "summary": "MOB Customer Pricelist",
    "category": "Generic Modules",
    "version": "1.0.0",
    "sequence": 1,
    "author": "Webkul Software Pvt. Ltd.",
    "license": "Other proprietary",
    "website": "https://store.webkul.com",
    "description": """
MOB Customer Pricelist
======================
Module will add magento product group and Tier price inside odoo product Pricelist
This module works very well with latest version of magento 1.9.* and Odoo 11.0
------------------------------------------------------------------------------
""",
    "depends": ['magento_bridge'],
    "data": [
        'views/product_view.xml',
        'security/ir.model.access.csv',
        'views/customer_pricelist_view.xml',
    ],
    "application": True,
    "installable": True,
    "auto_install": False,
    "pre_init_hook": "pre_init_check",
}
