# -*- coding: utf-8 -*-
##########################################################################
#
#	Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   "License URL : <https://store.webkul.com/license.html/>"
#
##########################################################################

from odoo import api, fields, models, _


class ProductPricelistItem(models.Model):
    _inherit = 'product.pricelist.item'

    product_variant_id = fields.Many2one('product.product', 'Product')

    @api.model
    def create(self, vals):
        if 'product_id' in vals:
            vals['product_variant_id'] = vals['product_id']
        return super(ProductPricelistItem, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'product_id' in vals:
            vals['product_variant_id'] = vals['product_id']
        return super(ProductPricelistItem, self).write(vals)


class ProductProduct(models.Model):
    _inherit = 'product.product'

    pricelist_item = fields.One2many(
        'product.pricelist.item',
        'product_variant_id',
        'Pricelist Item')
