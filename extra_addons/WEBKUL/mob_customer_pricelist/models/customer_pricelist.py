# -*- coding: utf-8 -*-
##########################################################################
#
#	Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   "License URL : <https://store.webkul.com/license.html/>"
#
##########################################################################

import datetime
try:
    from xmlrpc import client as xmlrpclib
except ImportError:
    import xmlrpclib
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class CustomerPricelist(models.Model):
    _name = "customer.pricelist"
    _description = "Customer Pricelist Mapping"

    name = fields.Char('Customer Group', size=64)
    pricelist_id = fields.Many2one('product.pricelist', 'Pricelist')
    mage_website_id = fields.Integer('Magento Website Id')
    mage_customer_group = fields.Integer('Magento Customer Group Id')
    create_date = fields.Datetime('Created Date', readonly=True)

    ############## Exporting Tier/Group Prices ############
    def exportTierAndGroupPrices(self, mapObj, url, session):
        """
         Export Pricelist Discount As a Tier/Group Price
        """
        flag = False
        context = dict(self._context or {})
        productObj = mapObj.pro_name
        mageProdId = mapObj.mag_product_id
        if mageProdId:
            tierPrices = []
            groupPrices = []
            server = xmlrpclib.Server(url)
            for itemObj in productObj.pricelist_item:
                pricelistId = itemObj.pricelist_id.id
                context['pricelist'] = pricelistId
                context['quantity'] = itemObj.min_quantity
                price = itemObj.fixed_price
                existPl = self.search([('pricelist_id', '=', pricelistId)])
                if existPl:
                    groupObj = existPl[0]
                    priceDiscount = {}
                    priceDiscount['website_id'] = groupObj.mage_website_id
                    priceDiscount['cust_group'] = groupObj.mage_customer_group
                    priceDiscount['price'] = round(price, 2)
                    if itemObj.min_quantity:
                        priceDiscount['price_qty'] = itemObj.min_quantity
                        tierPrices.append(priceDiscount)
                    else:
                        groupPrices.append(priceDiscount)

            if tierPrices:
                flag = server.call(
                    session, 'mobcustomergroup.add_tier_price', [
                        mageProdId, tierPrices])
            if groupPrices:
                flag = server.call(
                    session, 'mobcustomergroup.add_group_price', [
                        mageProdId, groupPrices])
        return flag

    def syncProductPrices(self):
        text = ''
        successIds = []
        failureIds = []
        activeIds = self._context.get('active_ids')
        connection = self.env['magento.configure']._create_connection()
        if connection:
            url = connection[0]
            session = connection[1]
            for productId in activeIds:
                search_ids = self.env['magento.product'].search(
                    [('pro_name', '=', productId)])
                if search_ids:
                    res = self.exportTierAndGroupPrices(
                        search_ids[0], url, session)
                    if res:
                        successIds.append(productId)
                    else:
                        failureIds.append(productId)
        if successIds:
            text = "All Tier/Group Prices of Product ids %s are successfully synchronized at Magento." % (
                successIds)
        if failureIds:
            text = text + \
                "\n\nNo Pricelist Discounts Found For Following Product Ids %s !!!" % (failureIds)
        partial = self.env['message.wizard'].create({'text': text})
        return {
            'name': _("Information"),
            'view_mode': 'form',
            'view_id': False,
            'view_type': 'form',
            'res_model': 'message.wizard',
            'res_id': partial.id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
        }

    ########### Exporting Customer Group On Magento ############
    @api.model
    def exportCustomerGroupAtMagento(self, mapObj, url, session):
        """
         Export Pricelist Discount As a Tier/Group Price
        """
        flag = False
        partnerObj = mapObj.cus_name
        mageCustomerId = mapObj.mag_customer_id
        if mageCustomerId:
            customerGroups = []
            server = xmlrpclib.Server(url)
            if partnerObj.property_product_pricelist:
                pricelistId = partnerObj.property_product_pricelist.id
                existPl = self.search([('pricelist_id', '=', pricelistId)])
                for groupObj in existPl:
                    websiteId = groupObj.mage_website_id
                    groupId = groupObj.mage_customer_group
                    customerGroups.append({str(websiteId): groupId})
                if customerGroups:
                    flag = server.call(
                        session, 'mobcustomergroup.add_customer_group', [
                            mageCustomerId, customerGroups])
        return flag

    @api.model
    def syncCustomerGroup(self):
        text = ''
        successIds = []
        failureIds = []
        context = self._context
        activeIds = context.get('active_ids')
        connection = self.env['magento.configure']._create_connection()
        if connection:
            url = connection[0]
            session = connection[1]
            for partnerId in activeIds:
                existPartner = self.env['magento.customers'].search(
                    [('cus_name', '=', partnerId)])
                if existPartner:
                    res = self.exportCustomerGroupAtMagento(
                        existPartner[0], url, session)
                    if res:
                        successIds.append(partnerId)
                    else:
                        failureIds.append(partnerId)
        if successIds:
            text = "Customer Group of Csutomer ids %s are successfully synchronized at Magento." % (
                successIds)
        if failureIds:
            text = text + \
                "\n\nCustomer Group doesn't synced At magento. Selected Customer Ids %s  pricelist is not yet mapped with any group!!!" % (failure_ids)
        partial = self.env['message.wizard'].create({'text': text})
        return {
            'name': _("Information"),
            'view_mode': 'form',
            'view_id': False,
            'view_type': 'form',
            'res_model': 'message.wizard',
            'res_id': partial.id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
        }
        return True

    @api.model
    def add_pricelist_discount(self, data):
        response = False
        for key in data:
            discountData = data[key]
            websiteId = discountData['website_id']
            custGroup = discountData['cust_group']
            existObjs = self.search(
                [('mage_website_id', '=', websiteId), ('mage_customer_group', '=', custGroup)])
            if existObjs:
                groupObj = existObjs[0]
                pricelistId = groupObj.pricelist_id.id
                itemId = self.create_pricelist_item(
                    pricelistId, discountData)
                if itemId:
                    response = True
        return response

    @api.model
    def create_pricelist_item(self, pricelistId, data):
        itemId = 0
        itemData = {}
        plItemModel = self.env['product.pricelist.item']
        if 'product_id' in data and data['product_id']:
            productId = data['product_id']
            itemData['fixed_price'] = data['price']
            itemData['min_quantity'] = data['price_qty']
            existObjs = plItemModel.search([('min_quantity', '=', data['price_qty']), (
                'product_variant_id', '=', productId), ('pricelist_id', '=', pricelistId)])

            if existObjs:
                existObjs[0].write(itemData)
            else:
                prodName = self.env['product.product'].browse(
                    productId).name or productId
                itemData['name'] = prodName + " Rule"
                itemData['applied_on'] = '0_product_variant'
                itemData['compute_price'] = 'fixed'
                itemData['product_id'] = productId
                itemData['pricelist_id'] = pricelistId
                itemId = plItemModel.create(itemData)
                itemId = itemId.id
        return itemId

    @api.model
    def create_customer_pricelist(self, data):
        """create and search pricelist by any webservice like xmlrpc.
        @param code: currency code.
        @return: pricelist_id
        """

        currencyObjs = self.env['res.currency'].search(
            [('name', '=', data['code'])]) or [False]
        if currencyObjs:
            pricelistDict = {
                'name': data['name'],
                'active': True,
                'currency_id': currencyObjs[0].id,
            }
            pricelistObj = self.env["product.pricelist"].create(pricelistDict)
            pricelistId = pricelistObj.id
            return pricelistId
        return False
