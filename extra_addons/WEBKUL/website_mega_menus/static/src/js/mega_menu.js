/* Copyright (c) 2016-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>) */
/* See LICENSE file for full copyright and licensing details. */
odoo.define('website_mega_menus.website_mega_menus', function (require) {
    "use strict";
    var base = require('web_editor.base');
    var ajax = require('web.ajax');

       $(document).ready(function () {
            $('.levelclass').each(function(index) {
                var level = $(this).attr('level')
                $(this).css({'padding-left':6*level+'%'})
            });
        $("#top_menu .dropdown").hover(function(){
               var color = $(this).find('.dropbtn').attr('color')
               var mega_menu = $(this).find('.dropbtn').attr('mega_menu')
               var width = window.innerWidth;
               if (mega_menu == 'True' && width > 767)
               {
                 $(this).find('.dropbtn').css({'border-bottom':'5px solid #'+color+ '' ,'padding-bottom': '10px'})
               }
            }, function() {
                $(this).find('.dropbtn').css({'border-bottom':'none','padding-bottom': '15px'})
            });

            $('.dropbtn').click(function(event){
              event.preventDefault();
              var width = window.innerWidth;
              var href = $(this).attr('href');
              if (width <= 767 && href== "#mega"){
                $(this).parent('li').find('.dropdown-content').toggle();
              }
              else{
                window.location.href = href;
              }
            })

            $('main').click(function() {
              $(".navbar-collapse").collapse('hide');
            })

        });



       });
