# -*- coding: utf-8 -*-
##########################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   License URL : <https://store.webkul.com/license.html/>
#
##########################################################################

try:
    from xmlrpc import client as xmlrpclib
except ImportError:
    import xmlrpclib

import binascii
import requests

from odoo import api, fields, models, _


class MobImageType(models.Model):
    _name = 'mob.image.type'
    
    name = fields.Char(string='Type')
    label = fields.Char(string='Label', size=100)
    
class MobExtraImage(models.Model):
    _name = 'mob.extra.image'

    name = fields.Char(string='Label', size=64)
    image = fields.Binary(string="Image")
    image_type = fields.Many2many('mob.image.type', 'product_image_type', 'image_id', 'type_id', string='Image Type')
    mage_file = fields.Char(string='Magento File Name')
    mage_product_id = fields.Integer(string='Magento Product Id', default=0)

    @api.model
    def create_image(self, mageProdId, productId, productType, imageList):
        imgIds = []
        if productId and productType and mageProdId:
            imgTypeModel = self.env['mob.image.type']
            for data in imageList:
                if 'types' in data:
                    types = data.get('types')
                    typeIds = []
                    for typ in types:
                        search_type = imgTypeModel.search([('name','=',typ)])
                        if search_type:
                            typeIds.append(search_type[0].id)
                        else:
                            typeIds.append(imgTypeModel.create({'name':typ}).id)
                    data.pop('types')
                    if typeIds:
                        data['image_type'] = [(6, 0, typeIds)]
                data['mage_product_id'] = mageProdId
                existImgObj = self.search(
                    [('mage_file','=',data.get('mage_file')),('mage_product_id','=',mageProdId)], limit=1)
                imageUrl = data.pop('image_url', False)
                if not existImgObj:
                    if imageUrl:
                        proImage = binascii.b2a_base64(requests.get(imageUrl).content)
                        data['image'] = proImage
                        imgIds.append(self.create(data).id)
                else:
                    imgIds.append(existImgObj.id)
            if imgIds:
                productObj = self.env['product.product'].browse(productId)
                if productType == 'product':
                    prodTemplObj = productObj.product_tmpl_id
                    line_ids = prodTemplObj.attribute_line_ids.ids					
                    if not line_ids and prodTemplObj:
                        prodTemplObj.tmpl_extra_image = [(6, 0, imgIds)]
                    else:
                        productObj.pro_extra_image = [(6, 0, imgIds)]
                elif productType == 'template':
                    prodTemplObj = productObj.product_tmpl_id
                    prodTemplObj.tmpl_extra_image = [(6, 0, imgIds)]
            return True
        return False

    @api.multi
    def unlink(self):
        ctx = dict(self._context) or {}
        if 'magento' not in ctx:
            connection = self.env['magento.configure']._create_connection()
            if connection:
                url = connection[0]
                session = connection[1]
                autoImgDel = connection[3]
                if autoImgDel == 'Yes':
                    server = xmlrpclib.Server(url)
                    for selfObj in self:
                        imageFile = selfObj.mage_file or False
                        mageProdId = selfObj.mage_product_id or False
                        if imageFile and mageProdId:
                            try :
                                test = server.call(session, 'product_media.remove', [mageProdId, imageFile])
                            except Exception as e:
                                pass
        return super(MobExtraImage, self).unlink()
