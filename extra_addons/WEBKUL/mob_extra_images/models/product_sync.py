# -*- coding: utf-8 -*-
##########################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   License URL : <https://store.webkul.com/license.html/>
#
##########################################################################

try:
    from xmlrpc import client as xmlrpclib
except ImportError:
    import xmlrpclib

from odoo import api, fields, models, _


class MagentoSynchronization(models.TransientModel):
    _inherit = 'magento.synchronization'

    #############################################
    ##  Inherited export Specific product sync ##
    #############################################

    @api.model
    def _export_specific_template(self , templateObj, url, session):
        pro = super(MagentoSynchronization, self)._export_specific_template(templateObj, url, session)
        if pro and pro[0] != 0:
            tmplExtraImage = templateObj.tmpl_extra_image
            productImage = templateObj.image
            self._export_update_product_extra_images(tmplExtraImage, pro[1], url, session, productImage)
        return pro
    
    @api.model
    def _export_specific_product(self, vrntObj, templateSku, url, session):
        pro = super(MagentoSynchronization, self)._export_specific_product(vrntObj, templateSku, url, session)
        if pro and pro[0] != 0:
            proExtraImage = vrntObj.pro_extra_image
            productImage = vrntObj.image
            self._export_update_product_extra_images(proExtraImage, pro[1], url, session, productImage)
        return pro

    ################# update extra image ###################
    
    @api.model
    def _export_update_product_extra_images(self, proExtraImage, mageProdId, url, session, productImage):
        server = xmlrpclib.Server(url)
        for imgObj in proExtraImage:
            if not imgObj.mage_file :
                types = []
                position = str(100 + imgObj.id)
                files = {'content':imgObj.image, 'mime':'image/jpeg'}	
                for imgId in imgObj.image_type:
                    types.append(imgId.name)			
                pic = {'file':files, 'label':imgObj.name, 'position':position, 'types':types, 'exclude':0}
                image = [mageProdId,pic]
                k = server.call(session, 'catalog_product_attribute_media.create', image)
                imgObj.write({'mage_file':k, 'mage_product_id':mageProdId})
        return True	

