# -*- coding: utf-8 -*-
##########################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   License URL : <https://store.webkul.com/license.html/>
#
##########################################################################

from odoo import api, fields, models, _


class ProductProduct(models.Model):
    _inherit = 'product.product'
    
    pro_extra_image = fields.Many2many(
        'mob.extra.image', 
        'pro_img_set', 
        'pro_id', 
        'image_id', 
        string='Extra Images')
