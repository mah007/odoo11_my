# -*- coding: utf-8 -*-
##########################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   License URL : <https://store.webkul.com/license.html/>
#
##########################################################################

from odoo import api, fields, models, _


class ProductTemplate(models.Model):
    _inherit = 'product.template'
    
    tmpl_extra_image = fields.Many2many(
        'mob.extra.image', 
        'tmp_img_set', 
        'tmpl_id', 
        'image_id', 
        string='Extra Images')

    @api.multi
    def write(self, vals):
        moduleMInst = self.env['ir.module.module'].search([("name","=","mob_multi_instance"),("state","=","installed")])
        tempMapModel = self.env['magento.product.template']
        magConfigModel = self.env['magento.configure']
        magSyncModel = self.env['magento.synchronization']
        instanceId = False
        ctx = dict(self._context or {})
        if moduleMInst:
            for tempObj in self:
                existMapObj = tempMapModel.search(
                    [("template_name","=", tempObj.id),("default_instance","=",True)], limit=1)
                if existMapObj:
                    instanceId = existMapObj.instance_id.id
        else:
            activeConnObj = self.env['magento.configure'].search([('active','=',True)], limit=1)
            if activeConnObj:
                instanceId = activeConnObj.id

        if 'tmpl_extra_image' in vals:
            imgBefore = self.tmpl_extra_image.ids
        res = super(ProductTemplate, self).write(vals)
        if 'tmpl_extra_image' in vals:
            imgAfter = self.tmpl_extra_image.ids
            imgNeedDel = list(set(imgBefore) - set(imgAfter))
            if  imgNeedDel:
                imgObjs = self.env['mob.extra.image'].browse(imgNeedDel)
                imgObjs.unlink()
            for tempObj in self:
                lineIds = tempObj.attribute_line_ids.ids	
                variantsObj = False
                if tempObj.product_variant_ids:
                    variantsObj = tempObj.product_variant_ids[0] 	
                imageIds = tempObj.tmpl_extra_image.ids		
                if not lineIds and variantsObj and imageIds:
                    variantsObj.pro_extra_image = [(6, 0, imageIds)]
                if 'magento' not in ctx:
                    tempMapObj = tempMapModel.search(
                        [("template_name", "=", tempObj.id) , ("instance_id", "=", instanceId)], limit=1)
                    if tempMapObj:
                        connection = magConfigModel.with_context(instance_id=instanceId)._create_connection()
                        if connection:
                            url = connection[0]
                            session = connection[1]
                            mageProdId = tempMapObj.mage_product_id
                            tmplExtraImage = tempObj.tmpl_extra_image
                            productImage = tempObj.image
                            magSyncModel._export_update_product_extra_images(
                                tmplExtraImage, mageProdId, url, session, productImage)
        return res

    @api.model
    def create(self, vals):
        tempObj = super(ProductTemplate, self).create(vals)
        lineIds = []
        if tempObj.attribute_line_ids:
            lineIds = tempObj.attribute_line_ids.ids	
        variantsObj = False
        if tempObj.product_variant_ids:
            variantsObj = tempObj.product_variant_ids[0]
        imageIds = tempObj.tmpl_extra_image.ids		
        if not lineIds and variantsObj and imageIds:
            variantsObj.pro_extra_image = [(6, 0, imageIds)]
        return tempObj
