# -*- coding: utf-8 -*-
##########################################################################
#
#   Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#   See LICENSE file for full copyright and licensing details.
#   License URL : <https://store.webkul.com/license.html/>
#
##########################################################################

from odoo import api, fields, models, _


class MagentoConfigure(models.Model):
    _inherit = 'magento.configure'
    
    auto_image_del = fields.Selection([
        ('Yes','Yes'),
        ('No','No')], 
        string='Auto Image Delete', 
        default='No', 
        required=True,
        help="If yes, then it'll delete corresponding magento images from products.")

    @api.model
    def _create_connection(self):
        """ Inherited _create_connection from mob base module """
        connection = super(MagentoConfigure, self)._create_connection()
        if connection:
            instanceId = connection[2]
            obj = self.browse(instanceId)
            autoImgDel = obj.auto_image_del
            connection.append(autoImgDel)
            return connection
