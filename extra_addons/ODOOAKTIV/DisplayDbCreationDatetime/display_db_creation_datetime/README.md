=========================
Display Database Creation
=========================

Version
=======

Odoo v11


Installation
============

This module will get automatically installed at a time of database creation. So there is no need to install it separately.


Configuration
=============

No extra configuration is require to use this module's functionality.
-> Just put the module in addons, run Odoo server and it will work in new database.
-> If you are planning to run it with existing db then follow below steps:
   Update addons with this module > Restart Odoo server > Update Apps list > Install


Usage
=====

-> When user visits the path /web/database/selector or /web/database/manager,it will display list of available databases along with their creation datetime info.


Bug Tracker
===========

In case of trouble, please contact us at sales@aktivsoftware.com
