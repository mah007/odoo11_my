# -*- coding: utf-8 -*-
# Part of AktivSoftware See LICENSE file for full copyright and licensing
# details.

{
    'name': 'Display Database Datetime',
    'version': '11.0.1.0.0',
    'license': 'AGPL-3',
    'summary': 'Display database creation datetime information',
    'description': """
        With the help of this module, user can view the list of available
        databases along with their creation datetime detail.
    """,
    'category': 'Tools',
    'author': "Aktiv Software",
    'website': "http://www.aktivsoftware.com",
    'depends': ['web'],
    'images': [
        'static/description/banner.jpg',
    ],
    'auto_install': True,
    'installable': True,
    'application': False
}

