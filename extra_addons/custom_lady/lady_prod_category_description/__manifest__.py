{
    "name": "Lady Category Description",
    "summary": """ """,
    "description": """Category Description for LadyShop""",
    "version": "0.1",
    "category": "Website",
    "website": "",
    "author": "Romanishen Vasya",
    "contributors": [],
    "depends": [
        "website",
        "website_sale"
    ],
    "data": [
        "views/assets.xml",
        "views/templates.xml",
        "views/product_public_category_view.xml"
    ],
    "demo": [],
    "qweb": [],
    "images": [],
    "application": False,
    "installable": True,
}