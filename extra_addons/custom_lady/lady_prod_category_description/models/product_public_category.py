# -*- coding: utf-8 -*-

from odoo import fields, models
from odoo.addons.base.res.res_partner import WARNING_MESSAGE, WARNING_HELP


class product_public_category(models.Model):
    _inherit = 'product.public.category'

    # category_description = fields.Html('Category Description')
    category_description = fields.Text('Category Description')