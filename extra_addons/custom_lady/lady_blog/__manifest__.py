{
    "name": "Lady Blog",
    "summary": """ """,
    "description": """Blog for LadyShop""",
    "version": "0.1",
    "category": "Website",
    "website": "",
    "author": "Romanishen Vasya",
    "contributors": [],
    "depends": [
        "website",
        "website_sale"
    ],
    "data": [
        "views/assets.xml",
        "views/templates.xml",
    ],
    "demo": [],
    "qweb": [

    ],
    "images": [],
    "application": False,
    "installable": True,
}