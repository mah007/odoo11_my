# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.tools import html2plaintext

import logging
_logger = logging.getLogger(__name__)

    
class BlogPostExt(models.Model):

    _inherit = "blog.post"
    _logger.info('==========BlogPostExt============')

    @api.multi
    @api.depends('content', 'teaser_manual')
    def _compute_teaser(self):
        for blog_post in self:
            if blog_post.teaser_manual:
                blog_post.teaser = blog_post.teaser_manual
            else:
                content = html2plaintext(blog_post.content).replace('\n', ' ')
                blog_post.teaser = content[:450] + '...'