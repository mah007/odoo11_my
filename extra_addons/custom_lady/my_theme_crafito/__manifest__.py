# -*- coding: utf-8 -*-
# Part of AppJetty. See LICENSE file for full copyright and licensing details.

{
    'name': 'My theme looks like Crafito',
    'summary': 'Advanced Responsive Theme with A Range of Custom Snippets',
    'description': ''' ''',
    # 'category': 'Theme',
    'version': '1.0.4',
    'author': 'Vasya Romanishen',
    'depends': [
        'website_sale',
    ],
    'data': [
        'views/assets.xml',
        'views/slider_views.xml',
        'views/snippets.xml',
        'views/theme_customize.xml',
    ],

    'application': True,
}