odoo.define('my_theme_crafito.crafito_frontend_js', function(require) {
    'use strict';

    var animation = require('website.content.snippets.animation');
    var ajax = require('web.ajax');
    var core = require('web.core');
    var _t = core._t;

    function getTimeRemaining(formated_date) {
        var t = Date.parse(formated_date) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }

    function initializeClock(id, endtime) {
        var clock = $(id)
        var test = getTimeRemaining(endtime);
        if (test.total > 0) {
            var timeinterval = setInterval(function() {
                var t = getTimeRemaining(endtime);
                clock.find('.days').text(t.days);
                clock.find('.hours').text(t.hours);
                clock.find('.minutes').text(t.minutes);
                clock.find('.seconds').text(t.seconds);
                if (t.total <= 0) {
                    clearInterval(timeinterval);
                }
            }, 1000);

        }
    }

    animation.registry.theme_crafito_product_slider = animation.Class.extend({

        selector: ".oe_prod_slider",
        start: function() {
            var self = this;
            if (this.editableMode) {
                var $prod_slider = $('#wrapwrap').find('#theme_crafito_custom_product_slider');
                var prod_name = _t("Products Slider")
                
                _.each($prod_slider, function (single){
                    $(single).empty().append('<div class="container">\
                                                    <div class="block-title">\
                                                        <h3 class="fancy">' + prod_name + '</h3>\
                                                    </div>\
                                                </div>')
                });
            }
            if (!this.editableMode) {
                var slider_id = self.$target.attr('data-prod-slider-id');
                $.get("/theme_crafito/product_get_dynamic_slider", {
                    'slider-id': self.$target.attr('data-prod-slider-id') || '',
                }).then(function(data) {
                    if (data) {
                        self.$target.empty();
                        self.$target.append(data);
                        $(".oe_prod_slider").removeClass('hidden');

                        ajax.jsonRpc('/theme_crafito/product_image_effect_config', 'call', {
                            'slider_id': slider_id
                        }).done(function(res) {
                            $('div#' + res.s_id).owlCarousel({
                                margin: 10,
                                responsiveClass: true,
                                items: res.counts,
                                loop: true,
                                nav: true,
                                navText : ['<div class="prev-elem"></div>', '<div class="next-elem"></div>'],
                                autoplay: res.auto_rotate,
                                autoplayTimeout:res.auto_play_time,
                                autoplayHoverPause:true,

                                autoHeight: false,
                                // autoHeightClass: 'owl-height',

                                responsive: {
                                    0: {
                                        items: 1,
                                    },
                                    420: {
                                        items: 2,
                                    },
                                    768: {
                                        items: 3,
                                    },
                                    1000: {
                                        items: res.counts,
                                    },
                                    1500: {
                                        items: res.counts,
                                    },
                                },
                            });
                            $('div#' + res.s_id).on('mousewheel', '.owl-stage', function (e) {
                                if (e.deltaY>0) {
                                    owl.trigger('next.owl');
                                } else {
                                    owl.trigger('prev.owl');
                                }
                                e.preventDefault();
                            });
                        });
                    }
                });
            }
        }
    });

});
