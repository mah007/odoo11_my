# -*- coding: utf-8 -*-

from odoo import api, fields, models

class ProductSlider(models.Model):
    _name = 'product.slider.config'

    name = fields.Char(string="Slider name", default='New Products', required=True,
                       translate=True,
                       help="Slider title to be displayed on website like Best Product, Latest and etc...")
    active = fields.Boolean(string="Active", default=True)
    no_of_counts = fields.Selection([('3', '3'), ('4', '4'), ('5', '5'), ('6', '6')], string="Counts",
                                    default='4', required=True,
                                    help="No of products to be displayed in slider.")

    auto_rotate = fields.Boolean(string='Auto Rotate Slider', default=True)
    sliding_speed = fields.Integer(string="Slider sliding speed", default='5000',
                                   help='Sliding speed of a slider can be set from here and it will be in milliseconds.')
    collections_products = fields.Many2many('product.template', 'theme_crafito_product_template_slider_rel',
                                            'slider_id', 'prod_id', string="Collections of products")