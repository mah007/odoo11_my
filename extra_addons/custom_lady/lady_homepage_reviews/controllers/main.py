# -*- coding: utf-8 -*-
import werkzeug
from odoo.addons.http_routing.models.ir_http import slug
from odoo import http
from odoo.http import route, request
from odoo.addons.website_mass_mailing.controllers.main import MassMailController
from odoo.addons.website.controllers.main import QueryURL

import json
import logging
_logger = logging.getLogger(__name__)


class LadyShopSliderSettings(http.Controller):
    
    @http.route(['/test_lady/product'], type='http', auth='public', website=True)
    def lady_product(self, **post):
        Prodacts = request.env['product.template'].sudo().search([], limit=10)
        
        _logger.info("=====================product=========================")
        
        values = []
        for item in Prodacts:
            product_messages = []
            messages = request.env['mail.message'].sudo().search(
                ['&', ('res_id', '=', item.id), ('model', '=', 'product.template')], 
                limit=2
            )

            for message in messages:                                                                 
                val = {
                    'author_id': message.author_id.name,
                    'body': message.body,
                    'message_id': message.message_id,
                }
                product_messages.append(val)

            val = {
                'product_id': item.id,
                'product_name': item.name,
                'product_price': item.website_price,
                'product_image': item.image_medium,
                'product_messages': product_messages,
            }
            values.append(val)   
        
        final = {
            'my_products': values
        }


        return request.render("lady_homepage_reviews.homepage_products_view", final)

    
class LadyshopEmployee(http.Controller):

    @http.route(['/test_lady/employee'], type='http', auth='public', website=True)
    def lady_employee(self, **post):
        Employee = request.env['res.partner'].sudo().search([('website_employee', '=', True)])
        
        _logger.info("======================my_employee========================")

        final = {
            'employees': Employee
        }

        return request.render("lady_homepage_reviews.employee_view", final)
