{
    "name": "Lady Homepage Reviews",
    "summary": """ """,
    "description": """Homepage Reviews for LadyShop""",
    "version": "0.1",
    "category": "Website",
    "website": "",
    "author": "Romanishen Vasya",
    "contributors": [],
    "depends": [
        "website",
        "website_sale",
        "crm",
    ],
    "data": [
        "views/assets.xml",
        "views/res_partner_view.xml",
        "views/templates.xml",
        "views/views.xml",
    ],
    "demo": [],
    "qweb": [],
    "images": [],
    "application": False,
    "installable": True,
    
}