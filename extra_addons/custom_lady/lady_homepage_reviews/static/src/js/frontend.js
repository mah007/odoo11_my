odoo.define('lady_homepage_reviews.homepage_frontend.js', function(require) {
    'use strict';
    var animation = require('website.content.snippets.animation');
    var core = require('web.core');
    var _t = core._t;

    animation.registry.lady_product = animation.Class.extend({
        selector: ".my_product_template",
        start: function() {
            var self = this;
            if (this.editableMode) {
                var $news2 = $('#wrapwrap').find('#ladyshop_homepage_my_products');
                var news_two_name = _t("My Products");

                _.each($news2, function (single){
                    $(single).empty().append('<div class="container">\
                                                    <div class="block-title">\
                                                        <h3 class="fancy">' + news_two_name + '</h3>\
                                                    </div>\
                                                </div>')
                });
            }
            if (!this.editableMode) {
                $.get("/test_lady/product").then(function(data) {
                    // console.log(data);
                    if (data) {
                        let $el_data = $(data);
                        let $carusel = $el_data.find('.carousel-content .col-md-8 p');
                        
                        $carusel.each((index, $item) => {
                            $($item).find('span.message').after( $( $($item).find('span.message').text() ));
                            $($item).find('span.message').remove();
                        });

                        self.$target.empty();
                        self.$target.append($el_data);
                        $(".my_product_template").removeClass('hidden');
                    }
                });
            }
        }
    });


    animation.registry.lady_employee = animation.Class.extend({
        selector: ".employee_template",
        start: function() {
            var self = this;
            if (this.editableMode) {
                var $news2 = $('#wrapwrap').find('#ladyshop_question');
                var news_two_name = _t("Employee");

                _.each($news2, function (single){
                    $(single).empty().append('<div class="container">\
                                                    <div class="block-title">\
                                                        <h3 class="fancy">' + news_two_name + '</h3>\
                                                    </div>\
                                                </div>')
                });
            }
            if (!this.editableMode) {
                $.get("/test_lady/employee").then(function(data) {
                    console.log('test employee');
                    if (data) {
                        self.$target.empty();
                        self.$target.append(data);
                        $(".my_product_template").removeClass('hidden');
                    }
                });
            }
        }
    });

});