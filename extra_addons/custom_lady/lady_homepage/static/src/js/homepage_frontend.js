odoo.define('lady_homepage.homepage_frontend.js', function(require) {
    'use strict';
    var animation = require('website.content.snippets.animation');
    var core = require('web.core');
    var _t = core._t;


    animation.registry.js_subscribe_ladyshop = animation.registry.subscribe.extend({
        selector: ".js_subscribe_ladyshop",

        _onClick: function () {
            var self = this;
            var $email = this.$target.find(".js_subscribe_email:visible");

            if ($email.length && !$email.val().match(/.+@.+/)) {
                this.$target.addClass('has-error');
                return false;
            }
            this.$target.removeClass('has-error');

            console.log( this.$target.children()[1].value );
        
            this._rpc({
                route: '/website_mass_mailing/ladyshop_subscribe',
    
                params: {
                    'list_id': this.$target.data('list-id'),
                    'email': $email.length ? $email.val() : false,
                    'name': this.$target.find('input.js_subscribe_email.form-control.subscribe_field_name')[0].value
                },
            }).then(function (subscribe) {
                console.log(subscribe);
                self.$target.find(".js_subscribe_email, .input-group-btn").addClass("hidden");
                self.$target.find(".alert").removeClass("hidden");
                self.$target.find('input.js_subscribe_email').attr("disabled", subscribe ? "disabled" : false);
                self.$target.attr("data-subscribe", subscribe ? 'on' : 'off');
            });
        }
    });

});
