{
    "name": "Lady_Footer",
    "summary": """ """,
    "description": """Footer for LadyShop""",
    "version": "0.1",
    "category": "Website",
    "website": "",
    "author": "Romanishen Vasya",
    "contributors": [],
    "depends": [
        "website",
        "website_sale"
    ],
    "data": [
        "views/assets.xml",
        "views/footer.xml",
    ],
    "demo": [],
    "qweb": [],
    "images": [],
    "application": False,
    "installable": True,
}