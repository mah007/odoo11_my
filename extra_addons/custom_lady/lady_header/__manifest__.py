{
    "name": "Lady Header",
    "summary": """ """,
    "description": """Header for LadyShop""",
    "version": "0.1",
    "category": "Website",
    "website": "",
    "author": "Romanishen Vasya",
    "contributors": [],
    "depends": [
        "website",
        "website_sale"
    ],
    "data": [
        "views/assets.xml",
        "views/header.xml",
    ],
    "demo": [],
    "qweb": [],
    "images": [],
    "application": False,
    "installable": True,
}